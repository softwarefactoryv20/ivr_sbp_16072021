<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String ServidorBus = additionalParams.get("ServidorBus");
    String PuertoBus = additionalParams.get("PuertoBus");  
    String param1 = additionalParams.get("ParamInput1");  
    String param2 = additionalParams.get("ParamInput2");
    String param3 = additionalParams.get("ParamInput3");  
	
	String param4 = "01"+param2;
	
    JSONObject result = new JSONObject();
    String strCallUUID = state.getString("CallUUID");

		String url = "http://"+ServidorBus+":"+PuertoBus+"/ebdperu.conector/rest/agenciaremota/ivrclienteagenciaremota";
		String Body = "{\"institucion\": \""+param1+"\",\"nrodocident\": \""+param4+"\",\"tipodocident\": \""+param3+"\"}";
		
	    setLog(strCallUUID + " IBCC-SBP :::: BCK_WS_AGENCIA_REMOTA_BEYOND" );	
	    
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Body = " + Body);
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("charset", "utf-8");
		con.setRequestProperty("apikey", "89d02f12ab242bd922af20fcc");
		byte[] input = Body.getBytes();
		con.setRequestProperty("Content-Length", "1091");
    	
    	OutputStream os = con.getOutputStream();
    	byte[] input2 = Body.getBytes("utf-8");
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
    	
   		//print result
		String tempString = response.toString();
		String CodigoRetorno = "NULL";
		String TipDOIClie = "NULL";
		String NumDOI = "NULL";
		String NomClie = "NULL";
		String ApeClie = "NULL";
		String DesSeg = "NULL";
		String CDRAgeRemAsi = "NULL";
		String FunAsi = "NULL";
		String CamAdi1 = "NULL";
		
		try{
		JSONObject jsonobj = new JSONObject(tempString);
		CodigoRetorno = jsonobj.getString("error");
		TipDOIClie = jsonobj.getString("TipDOIClie");	
		NumDOI = jsonobj.getString("NumDOI");	
		NomClie = jsonobj.getString("NomClie");	
		ApeClie = jsonobj.getString("ApeClie");	
		DesSeg = jsonobj.getString("DesSeg");	
		CDRAgeRemAsi = jsonobj.getString("CDRAgeRemAsi");
		FunAsi = jsonobj.getString("FunAsi");
		CamAdi1 = jsonobj.getString("CamAdi1");	
			
		}catch(JSONException Error){
		setLog(strCallUUID + " IBCC-SBP :::: Response Error Json = " + Error);
		}
		
		CodigoRetorno = CodigoRetorno.trim();
		TipDOIClie = TipDOIClie.trim();
		DesSeg = DesSeg.trim();
		CDRAgeRemAsi = CDRAgeRemAsi.trim();
		FunAsi = FunAsi.trim();
		CamAdi1 = CamAdi1.trim();
		
		setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + tempString);	
		setLog(strCallUUID + " IBCC-SBP :::: Response CodigoRetorno = " + CodigoRetorno);	
		setLog(strCallUUID + " IBCC-SBP :::: Response TipDOIClie = " + TipDOIClie);
		setLog(strCallUUID + " IBCC-SBP :::: Response NumDOI = " + NumDOI);
		setLog(strCallUUID + " IBCC-SBP :::: Response NomClie = " + NomClie);
		setLog(strCallUUID + " IBCC-SBP :::: Response ApeClie = " + ApeClie);
		setLog(strCallUUID + " IBCC-SBP :::: Response DesSeg = " + DesSeg);
		setLog(strCallUUID + " IBCC-SBP :::: Response CDRAgeRemAsi = " + CDRAgeRemAsi);
		setLog(strCallUUID + " IBCC-SBP :::: Response FunAsi = " + FunAsi);
		setLog(strCallUUID + " IBCC-SBP :::: Response CamAdi1 = " + CamAdi1);			
	
	result.put("ParamOut1", CodigoRetorno);
	result.put("ParamOut2", TipDOIClie);
	result.put("ParamOut3", NumDOI);
	result.put("ParamOut4", NomClie+" "+ApeClie);
	result.put("ParamOut6", DesSeg);
	result.put("ParamOut7", CDRAgeRemAsi);
	result.put("ParamOut8", FunAsi);
	result.put("ParamOut9", CamAdi1);
	result.put("ParamOut10", param2);

    return result;

};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>

<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>

<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>