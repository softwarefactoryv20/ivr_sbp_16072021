<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    String Token = additionalParams.get("LSW_Token");
    String param4 = additionalParams.get("nroDocumento");
    String ServidorBus = additionalParams.get("LSW_Servidor_Bus");
    String PuertoBus = additionalParams.get("LSW_Puerto_BUS");
    String strCallUUID = state.getString("CallUUID");
    setLog(strCallUUID + " IBCC-SBP :::: Authorization = " + Token);
    setLog(strCallUUID + " IBCC-SBP :::: DNI = " + param4);
    
    JSONObject result = new JSONObject();
	
		String url = "https://"+ServidorBus+":"+PuertoBus+"/gssbp-creditcard/creditcards/v1";
		String Body = "{\"documentNumber\": \""+param4+"\", \"documentType\": \"1\",\"status\": \"A\"}";
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Body = " + Body);
    
    TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        
        // Install the all-trusting trust manager
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("charset", "utf-8");
		con.setRequestProperty("Authorization", Token);
		byte[] input = Body.getBytes();
		//String bodylen = new String(input.length());
		con.setRequestProperty("Content-Length", "80");
    	
    	OutputStream os = con.getOutputStream();
    	byte[] input2 = Body.getBytes("utf-8");
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    	StringBuilder builder = new StringBuilder();
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		String tempString = response.toString();
		String joy = "NULL";
		String agentAttention = "NULL";
		String digitalKey = "NULL";
		String count = "0";
		
		String cardNumber = "NULL";
		String virtualCardNumber = "NULL";
		String status = "NULL";

		try{
		JSONObject jsonobj = new JSONObject(tempString);
		joy = jsonobj.getString("joy");
		agentAttention = jsonobj.getString("agentAttention");
		digitalKey = jsonobj.getString("digitalKey");
		count = jsonobj.getString("count");
		JSONArray arraydata = jsonobj.getJSONArray("data");
			for(int i = 0; i < arraydata.length(); i++){
			   JSONObject objdata = arraydata.getJSONObject(i);
			   cardNumber = objdata.getString("cardNumber");
			   virtualCardNumber = objdata.getString("virtualCardNumber");
			   status = objdata.getString("status");
			}
		}catch(JSONException Error){
		setLog(strCallUUID + " IBCC-SBP :::: Response Error Json = " + Error);
		}

   		//print result
		setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + tempString);	
		setLog(strCallUUID + " IBCC-SBP :::: Response Joy  = " + joy);	
    	setLog(strCallUUID + " IBCC-SBP :::: Response agentAttention  = " + agentAttention);	
    	setLog(strCallUUID + " IBCC-SBP :::: Response digitalKey  = " + digitalKey);	
    	setLog(strCallUUID + " IBCC-SBP :::: Response count  = " + count);	
    	
    	setLog(strCallUUID + " IBCC-SBP :::: Response cardNumber  = " + cardNumber);
    	setLog(strCallUUID + " IBCC-SBP :::: Response virtualCardNumber  = " + virtualCardNumber);
    	setLog(strCallUUID + " IBCC-SBP :::: Response status  = " + status);
    	
    
    result.put("P3_agenteAttention", agentAttention);
    result.put("P3_count", count);
    result.put("P3_joy", joy);
    result.put("P3_digitalKey", digitalKey);
    result.put("P3_cardNumber", cardNumber);
    result.put("P3_virtualCardNumber", virtualCardNumber);
    result.put("P3_status", status);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>