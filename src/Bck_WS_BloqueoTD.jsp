<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String param1 = additionalParams.get("Paraminput1");
    String param2 = additionalParams.get("Paraminput2");
    String ServidorBus = additionalParams.get("Paraminput3");
    String PuertoBus = additionalParams.get("Paraminput4");
    String TipoBloq = additionalParams.get("Paraminput5");
    String ServidorWAS = additionalParams.get("Paraminput6");

    String strCallUUID = state.getString("CallUUID");
    setLog(strCallUUID + " IBCC-SBP :::: Authorization = " + param1);
    
    JSONObject result = new JSONObject();

		String url = "https://"+ServidorBus+":"+PuertoBus+"/gssbp-customer-product/debitcard/block/v2";
		String Body = "{\"cardNumber\": \""+param2+"\", \"reasonBlock\": \""+TipoBloq+"\"}";
		
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Body = " + Body);
    
    TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        
        // Install the all-trusting trust manager
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("charset", "utf-8");
		con.setRequestProperty("Authorization", param1);
		byte[] input = Body.getBytes();
		//String bodylen = new String(input.length());
		con.setRequestProperty("Content-Length", "80");
    	
    	OutputStream os = con.getOutputStream();
    	byte[] input2 = Body.getBytes("utf-8");
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
    	
   		//print result
		String tempString = response.toString();
		String codError = "NULL";
		String blockCode = "NULL";
		String switchDate = "NULL";
		String switchHour = "NULL";
		
		try{
		JSONObject jsonobj = new JSONObject(tempString);
		codError = jsonobj.getString("codError");
		blockCode = jsonobj.getString("blockCode");
		switchDate = jsonobj.getString("switchDate");
		switchHour = jsonobj.getString("switchHour");		
		}catch(JSONException Error){
		setLog(strCallUUID + " IBCC-SBP :::: Response Error Json = " + Error);
		}
		
		switchHour = switchHour.replace(":","");
		
		setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + tempString);
		setLog(strCallUUID + " IBCC-SBP :::: Response codError  = " + codError);
		setLog(strCallUUID + " IBCC-SBP :::: Response blockCode  = " + blockCode);	
    	setLog(strCallUUID + " IBCC-SBP :::: Response switchDate  = " + switchDate);
    	setLog(strCallUUID + " IBCC-SBP :::: Response switchHour  = " + switchHour);	
    
    String fecha = switchDate;
    String fechafinal = "";
    String horaBloq = switchHour;
    horaBloq = horaBloq.replace(":","");
    String anio = fecha.substring(0,4);
    String mes = fecha.substring(4,6);
    String dia = fecha.substring(6,8);
    String hora = horaBloq.substring(0,2);
    String Minuto = horaBloq.substring(2,4);
    String nuevodia = "http://"+ServidorWAS+":8080/APP_IVR_PeruSBP/AudiosIBCC/FECHA/days/m.day"+dia+".wav";
    String nuevomes = "http://"+ServidorWAS+":8080/APP_IVR_PeruSBP/AudiosIBCC/FECHA/Months/"+mes+".wav";
    String nuevoanio = "http://"+ServidorWAS+":8080/APP_IVR_PeruSBP/AudiosIBCC/FECHA/Year/"+anio+".wav";
    String horas = "http://"+ServidorWAS+":8080/APP_IVR_PeruSBP/Resources/Prompts/en-US/ordinals/0"+hora+".vox";
    String Minutos = "http://"+ServidorWAS+":8080/APP_IVR_PeruSBP/Resources/Prompts/en-US/ordinals/0"+Minuto+".vox";
    
    setLog(strCallUUID + " IBCC-SBP :::: Response fecha dia  = " + nuevodia);
    setLog(strCallUUID + " IBCC-SBP :::: Response fecha mes  = " + nuevomes);
    setLog(strCallUUID + " IBCC-SBP :::: Response fecha anio  = " + nuevoanio);
    setLog(strCallUUID + " IBCC-SBP :::: Response fecha horas  = " + horas);
    setLog(strCallUUID + " IBCC-SBP :::: Response fecha minutos  = " + Minutos);
    
    
    result.put("ParamOut", tempString);
    result.put("ParamOut1", blockCode);
    result.put("ParamOut2", fechafinal);
    result.put("ParamOut3", switchHour);
    result.put("ParamOut4", codError);
    result.put("ParamOut5", nuevodia);
    result.put("ParamOut6", nuevomes);
    result.put("ParamOut7", nuevoanio);
    result.put("ParamOut8", horas);
    result.put("ParamOut9", Minutos);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>

<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>

<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>