<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="java.io.File"%>
<%@page import="java.util.*"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String strCallUUID = state.getString("CallUUID");
	setLog(strCallUUID + " IBCC-SBP :::: Lee XML ConfiguracionAudio_SBP.xml");
	String nc_DNIS = additionalParams.get("nc_DNIS");
    JSONObject result = new JSONObject();
    
    String path = System.getProperty("user.dir");
    String archivo = "";
    String activo = "";
    String ruta = "";
    try {		    	
    	ruta = "D:\\EmergenciaIVR\\SBP\\ConfiguracionAudio_SBP.xml";
    	setLog(strCallUUID + " IBCC-SBP :::: Ruta XML = " + ruta);
		File fXmlFile = new File(ruta);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("Audio");
        int tamLista = nList.getLength();
        for (int temp = 0; temp < tamLista; temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                activo = eElement.getElementsByTagName("Activo").item(0).getTextContent();
                if(activo.equals("SI")){
                    archivo =  eElement.getElementsByTagName("NombreArchivo").item(0).getTextContent();
                    temp = tamLista;
                }
            }
        }
	 } 
	 catch(Exception e) {
            archivo = "";
            setLog(strCallUUID + " IBCC-SBP :::: Error Leer XML = " + e.toString());
     }
    setLog(strCallUUID + " IBCC-SBP :::: Archivo = " + archivo);
     result.put("audioInformativo_Sub_Opc", archivo);
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>