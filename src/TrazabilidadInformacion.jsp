<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.File"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.transform.Transformer"%>
<%@page import="javax.xml.transform.OutputKeys"%>
<%@page import="javax.xml.transform.TransformerException"%>
<%@page import="javax.xml.transform.TransformerFactory"%>
<%@page import="javax.xml.transform.dom.DOMSource"%>
<%@page import="javax.xml.transform.stream.StreamResult"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="java.io.IOException"%>
<%@page import="org.xml.sax.SAXException"%>
<%@page import="javax.xml.parsers.ParserConfigurationException"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    JSONObject result = new JSONObject();
    
    try {
        
        Document document 	= null;
        
        String callUuid 	= additionalParams.get("ParamName1");
        String directorio 	= additionalParams.get("ParamName2");
        String xmlFilePath 	= "D:/TramaIVR/" + callUuid + ".XML";
        String opcion 		= additionalParams.get("ParamName3");
        String menu			= additionalParams.get("ParamName4");
        String vdn			= additionalParams.get("ParamName5");
        
        String proceso              = "";
        String grupoTransferencia   = vdn;
        String nombrePlataforma     = "APP_IVR_PeruSBP";        
        
        String opcionMenu           = opcion;
        String idProgramacion       = "2";
        String idMenu               = menu;
        String idMenuPadre          = "";
        String idAudio              = "";
        String idAudioOpcion        = "";
        String tipoRegistro         = "";
        String idProceso            = "";
        String montoRecarga         = "";
        String idAudioTx            = "";
        String idGrupoTx            = "";
        String subMenu              = "";
        String estado               = "";
        String idEmp                = "4";
        String error                = "";

        String idIvr                = "2";
        String dateformat    = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
        String Fecha            = sdf.format(new Date());
        
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(new File(xmlFilePath));
            document.getDocumentElement().normalize();
        } 
        catch (IOException e) {
        	 
        }
        catch (ParserConfigurationException e) {
        	
        }
        catch (SAXException e) {
        	
        }
                
        NodeList nodoRaiz = document.getDocumentElement().getElementsByTagName("Options");
        Element Options = document.createElement("Option");
        
        Element EidProgramacion = document.createElement("idProgramación");EidProgramacion.setTextContent(idProgramacion);
        Element EidMenu = document.createElement("idMenu");EidMenu.setTextContent(menu);
        Element Emenu = document.createElement("menu");Emenu.setTextContent(menu);
        Element EidMenuPadre = document.createElement("idMenuPadre");EidMenuPadre.setTextContent(idMenuPadre);
        Element EidAudio = document.createElement("idAudio");EidAudio.setTextContent(idAudio);
        Element EidAudioOpcion = document.createElement("idAudioOpcion");EidAudioOpcion.setTextContent(idAudioOpcion);
        Element EopcionMenu = document.createElement("opcionMenu");EopcionMenu.setTextContent(opcionMenu);
        Element EtipoRegistro = document.createElement("tipoRegistro");EtipoRegistro.setTextContent(tipoRegistro);
        Element EidProceso = document.createElement("idProceso");EidProceso.setTextContent(idProceso);
        Element Eproceso = document.createElement("proceso");Eproceso.setTextContent(proceso);
        Element EmontoRecarga = document.createElement("montoRecarga");EmontoRecarga.setTextContent(montoRecarga);
        Element EidAudioTx = document.createElement("idAudioTx");EidAudioTx.setTextContent(idAudioTx);
        Element EidGrupoTx = document.createElement("idGrupoTx");EidGrupoTx.setTextContent(idGrupoTx);
        Element EgrupoTransferencia = document.createElement("grupoTransferencia");EgrupoTransferencia.setTextContent(grupoTransferencia);
        Element EnombrePlataforma = document.createElement("nombrePlataforma");EnombrePlataforma.setTextContent(nombrePlataforma);
        Element EsubMenu = document.createElement("subMenu");EsubMenu.setTextContent(subMenu);
        Element Eestado = document.createElement("estado");Eestado.setTextContent(estado);
        Element EidEmp = document.createElement("idEmp");EidEmp.setTextContent(idEmp);
        Element EidIvr = document.createElement("idIvr");EidIvr.setTextContent(idIvr);
        Element Eerror = document.createElement("error");Eerror.setTextContent(error);
        Element EDate = document.createElement("Date");EDate.setTextContent(Fecha);
        
        Options.appendChild(EidProgramacion);
        Options.appendChild(EidMenu);
        Options.appendChild(Emenu);
        Options.appendChild(EidMenuPadre);
        Options.appendChild(EidAudio);
        Options.appendChild(EidAudioOpcion);
        Options.appendChild(EopcionMenu);
        Options.appendChild(EtipoRegistro);
        Options.appendChild(EidProceso);
        Options.appendChild(Eproceso);
        Options.appendChild(EmontoRecarga);
        Options.appendChild(EidAudioTx);
        Options.appendChild(EidGrupoTx);
        Options.appendChild(EgrupoTransferencia);
        Options.appendChild(EnombrePlataforma);
        Options.appendChild(EsubMenu);
        Options.appendChild(Eestado);
        Options.appendChild(EidEmp);
        Options.appendChild(EidIvr);
        Options.appendChild(Eerror);
        Options.appendChild(EDate);
        
        nodoRaiz.item(0).appendChild(Options);
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File(xmlFilePath));

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        transformer.transform(domSource, streamResult);

    }catch(Exception ex){
    	
    }
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/backend.jspf" %>