<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Encripta.IvrString.Encripta"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	///-- INICIO - Leer Archivo de Configuracion
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	String nc_DNIS = additionalParams.get("nc_DNIS");
	String Segmento = additionalParams.get("Segmento");

	Logger log = Logger.getLogger("Bck_Asig_Segmentacion.jsp");
	setLog(strCallUUID + " IBCC-SBP :::: Primera linea de LOG Bck_Asig_Segmentacion.jsp");
	String SBP_Seg_Preferente_VDN = additionalParams.get("SBP_Seg_Preferente_VDN");;
	String SBP_Seg_Personal_VDN = additionalParams.get("SBP_Seg_Personal_VDN");;
	String SBP_Seg_Estandar_VDN = additionalParams.get("SBP_Seg_Estandar_VDN");;
	String SBP_Seg_Otros_VDN = additionalParams.get("SBP_Seg_Otros_VDN");;
	String SBP_Seg_Cli_VDN = SBP_Seg_Otros_VDN;
	String SBP_Prioridad_VDN_1 = additionalParams.get("SBP_Prioridad_VDN_1");;
	String SBP_Prioridad_VDN_SEG_30 = additionalParams.get("SBP_Prioridad_VDN_SEG_30");;
	String ACTIVAR_BEYOND_SEG_30 = additionalParams.get("ACTIVAR_BEYOND_SEG_30");;
		
		if(Segmento.equals("32")){
			SBP_Seg_Cli_VDN = SBP_Seg_Preferente_VDN;
		}
		if(Segmento.equals("33")){
			SBP_Seg_Cli_VDN = SBP_Seg_Personal_VDN;
		}
		if(Segmento.equals("34")){
			SBP_Seg_Cli_VDN = SBP_Seg_Estandar_VDN;
		}
		
		
			if(Segmento.equals("30")){
				
				SBP_Seg_Cli_VDN = SBP_Prioridad_VDN_SEG_30;
			}
			if(Segmento.equals("31")){
				SBP_Seg_Cli_VDN = SBP_Prioridad_VDN_1;
				}
				if(Segmento.equals("2") || Segmento.equals("3") || Segmento.equals("4") || Segmento.equals("5") || Segmento.equals("13") || Segmento.equals("15") || Segmento.equals("16") || Segmento.equals("21")  || Segmento.equals("99")){
		    SBP_Seg_Cli_VDN = SBP_Seg_Otros_VDN;
			}
		
		
		
		
    setLog(strCallUUID + " IBCC-SBP :::: Segmento: " + Segmento);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_PREFERENTE_VDN: " + SBP_Seg_Preferente_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_PERSONAL_VDN: " + SBP_Seg_Personal_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_ESTANDAR_VDN: " + SBP_Seg_Estandar_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_OTROS_VDN: " + SBP_Seg_Otros_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_Cli_VDN: " + SBP_Seg_Cli_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_Prioridad_VDN_SEG_30: " + SBP_Prioridad_VDN_SEG_30);
	setLog(strCallUUID + " IBCC-SBP :::: ACTIVAR_BEYOND_SEG_30: " + ACTIVAR_BEYOND_SEG_30);
    JSONObject result = new JSONObject();    
    
    result.put("SBP_Seg_Cli_VDN", SBP_Seg_Cli_VDN);

    return result;    
};

public String consultarIp() throws UnknownHostException {
	InetAddress address = InetAddress.getLocalHost();
	 byte[] bIPAddress = address.getAddress();
	 String sIPAddress = "";
		 for (int x=0; x< bIPAddress.length; x++) {
			 if (x > 0) {
				 sIPAddress += ".";
			 }
		 sIPAddress += bIPAddress[x] & 255;
		 }
	return "//"+sIPAddress;
}
	
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>