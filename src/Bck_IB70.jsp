<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="middleware_support.classes.Saldo"%>
<%@page import="middleware_support.tramas.Bean_IB73_IB76_IB77_IB94"%>
<%@page import="middleware_support.tramas.Bean_IB73"%>
<%@page import="IvrTransaction.modelo.ValNroDocIdent"%>
<%@page import="IvrTransaction.Controller.ValNroDocIdentControllerIB7A"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	
		String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	
        setLog(strCallUUID + " IBCC-SBP :::: ================= INICIO.jsp IB ACKEND INVOCACION - IB7A : VALIDACION DE DOCUMENTO ======================");
		
		Logger log = Logger.getLogger("Bck_IB70.jsp");
		//Input
		String vInstitucion=additionalParams.get("codigoInst") ;  
		String vTarjeta=additionalParams.get("vNumDoc");
		String vTipoDocumento=additionalParams.get("vTipoDoc");
		
		setLog(strCallUUID + " IBCC-SBP :::: ID vInstitucion   		" + vInstitucion);
		setLog(strCallUUID + " IBCC-SBP :::: ID vdocumento   		" + vTarjeta);
		setLog(strCallUUID + " IBCC-SBP :::: ID vTipoDocumento   		" + vTipoDocumento);
		
		//Output
		String nroDocIdent="";
		String client_desc="";
		String city_desc="";
		String ctry_desc="";
		String tipoIdent = "";
		String codTipIdent = "";
		String flagClientCampana = "";
		String flagTarjetaBloqueada = "";
		String ctaBT = "";
		String id_num = "";
		String cif_key = "";
		int vCodErrorRpta = 0;
		String vCodRpta="0000"; 
		
		JSONObject result = new JSONObject();		
		
		ValNroDocIdentControllerIB7A ValDoc=new ValNroDocIdentControllerIB7A();
		ValNroDocIdent ValNroDocIdent=ValDoc.QIvrValNroDocIdent(vInstitucion, vTarjeta,vTipoDocumento);
		
		setLog(strCallUUID + " IBCC-SBP :::: ================= BACKEND INVOCACION - IB7A : VALIDACION DE DOCUMENTO ======================");
		
		vCodErrorRpta=ValDoc.getCodRetorno();
		if (vCodErrorRpta == 0) {  
			
			vCodRpta=ValNroDocIdent.getERROR();
			
			if (vCodRpta.equals("0000")) {
				id_num = ValNroDocIdent.getNroDocIdent().trim();
				nroDocIdent = ValNroDocIdent.getNroDocIdent().trim();
				tipoIdent = ValNroDocIdent.getTipoIdent().trim();
				codTipIdent = ValNroDocIdent.getCodTipIdent().trim();
				client_desc = ValNroDocIdent.getNombreCliente().trim();
				city_desc = ValNroDocIdent.getCiudad().trim();
				ctry_desc = ValNroDocIdent.getPais().trim();
				flagClientCampana = ValNroDocIdent.getFlagClientCampana().trim();
				flagTarjetaBloqueada = ValNroDocIdent.getFlagTarjetaBloqueada().trim();
				ctaBT = ValNroDocIdent.getCtaBT().trim();
				cif_key = ValNroDocIdent.getCtaBT().trim();
			
				setLog(strCallUUID + " IBCC-SBP :::: ID NUM   		" + id_num);
				setLog(strCallUUID + " IBCC-SBP :::: Nro de Identificacion   		" + nroDocIdent);
				setLog(strCallUUID + " IBCC-SBP :::: Tipo de Identificacion   		" + tipoIdent);
				setLog(strCallUUID + " IBCC-SBP :::: Codigo Tipo de Identificacion   " + codTipIdent);
				setLog(strCallUUID + " IBCC-SBP :::: Nombre del Cliente              " + client_desc);
				setLog(strCallUUID + " IBCC-SBP :::: Ciudad del Cliente		   		" + city_desc);
				setLog(strCallUUID + " IBCC-SBP :::: Pais del Cliente 		 		" + ctry_desc);
				setLog(strCallUUID + " IBCC-SBP :::: Cliente Flag Cliente Campaña	" + flagClientCampana);
				setLog(strCallUUID + " IBCC-SBP :::: Cliente Flag Tarjeta Bloqueada	" + flagTarjetaBloqueada);
				setLog(strCallUUID + " IBCC-SBP :::: Cuenta BT 						" + ctaBT);
				setLog(strCallUUID + " IBCC-SBP :::: CIF KEY						" + cif_key);
			
			}
		}
				
		setLog(strCallUUID + " IBCC-SBP :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
		setLog(strCallUUID + " IBCC-SBP :::: Codigo Retorno Transaccion OK = " + vCodRpta);
		
		
		result.put("id_num", id_num);
		result.put("nroDocIdent", nroDocIdent);
		result.put("vCodErrorRpta", vCodErrorRpta);
		result.put("vCodRpta", vCodRpta);
		result.put("client_desc", client_desc);
		result.put("city_desc", city_desc);
		result.put("ctry_desc", ctry_desc);
		result.put("tipoIdent", tipoIdent);
		result.put("flagClientCampana", flagClientCampana);
		result.put("flagTarjetaBloqueada", flagTarjetaBloqueada);
		result.put("ctaBT", ctaBT);
		result.put("cif_key", cif_key);
		result.put("codTipIdent", codTipIdent);
		
		return result;
	    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>