<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Encripta.IvrString.Encripta"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	///-- INICIO - Leer Archivo de Configuracion
	
	System.setProperty("https.protocols", "TLSv1.2");
	
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	String nc_DNIS = additionalParams.get("nc_DNIS");
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Configuracion.jsp");
	setLog(strCallUUID + " IBCC-SBP :::: Primera linea de LOG Configuracion.jsp");
	//--- INICIO vdn_cabinas.xml
	
	String path_cabina = null;
	try {
		
		path_cabina = consultarIp() + "/compartido/inicio_Cabinas.xml";
		setLog(strCallUUID + " IBCC-SBP :::: Ruta XML Cabinas = " + path_cabina);
	} catch (UnknownHostException e1) {
		e1.printStackTrace();
	}
	java.io.File file_cabina = new java.io.File(path_cabina);
	Properties properties_cabina = new Properties();
	java.io.InputStream inputStream_cab=null;
	
	if(file_cabina.exists()){
		properties_cabina.clear();
		
		
		try {
			inputStream_cab = new java.io.FileInputStream(file_cabina);
		    } 
		catch (FileNotFoundException e) {
			e.printStackTrace(); }
		
		try {
			properties_cabina.loadFromXML(inputStream_cab);
		    } 
		catch (InvalidPropertiesFormatException e) {
 			 e.printStackTrace(); } 
		catch (IOException e) {
			e.printStackTrace(); }		
	}
	String SBP_VDN_Cabinas = properties_cabina.getProperty("SBP_VDN_Cabinas");
	String SBP_VDN_Stardar_3116040 = properties_cabina.getProperty("SBP_VDN_Stardar_3116040");
	String SBP_VDN_Stardar_65533 = properties_cabina.getProperty("SBP_VDN_Stardar_65533");
	setLog(strCallUUID + " IBCC-SBP :::: VDN - Banca Telefonica o Cabinas: " + SBP_VDN_Cabinas);
	setLog(strCallUUID + " IBCC-SBP :::: VDN - Banca Telefonica o Cabinas 3116040: " + SBP_VDN_Stardar_3116040);
	setLog(strCallUUID + " IBCC-SBP :::: VDN - Banca Telefonica o Cabinas 65533: " + SBP_VDN_Stardar_65533);
	
	//--- FIN vdn_cabinas.xml
	
	String path=null;
	try {
		
		if(nc_DNIS.equals(SBP_VDN_Cabinas)){
			path = consultarIp() + "/compartido/inicio_SBP_Cab.xml";
		}else if(nc_DNIS.equals(SBP_VDN_Stardar_3116040)){
			path = consultarIp() + "/compartido/inicio_SBP_Cab_040.xml";
		}else{
			path = consultarIp() + "/compartido/inicio_SBP.xml";
		}
		setLog(strCallUUID + " IBCC-SBP :::: Ruta XML = " + path);
	} catch (UnknownHostException e1) {
		e1.printStackTrace();
	}
	java.io.File file = new java.io.File(path);
	Properties properties = new Properties();
	java.io.InputStream inputStream=null;
	
	if(file.exists()){
		properties.clear();
		
		try {
			inputStream = new java.io.FileInputStream(file);
		    } 
		catch (FileNotFoundException e) {
			e.printStackTrace(); }
		
		try {
			properties.loadFromXML(inputStream);
		    } 
		catch (InvalidPropertiesFormatException e) {
 			 e.printStackTrace(); } 
		catch (IOException e) {
			e.printStackTrace(); }		
	}
	String servidorwas = properties.getProperty("SBP_servidorwas");
	String TrxAgente = properties.getProperty("SBP_VDN_TrxAgente");
	String TransActivaMP = properties.getProperty("SBP_TransActivaMP");
	String TransActivaMP_Opc0 = properties.getProperty("SBP_TransActivaMP_Opc0");
	String TransActivaMP_Opc1 = properties.getProperty("SBP_TransActivaMP_Opc1");
	String TransActivaMP_Opc2 = properties.getProperty("SBP_TransActivaMP_Opc2");
	String TransActivaMP_Opc3 = properties.getProperty("SBP_TransActivaMP_Opc3");
	String TransActivaMP_Opc4 = properties.getProperty("SBP_TransActivaMP_Opc4");
	String TransActivaMP_OpcM = properties.getProperty("SBP_TransActivaMP_OpcM");
	String servidor_bd = properties.getProperty("SBP_servidor_bd");
	String usuario = properties.getProperty("SBP_usuario");
	String clave = properties.getProperty("SBP_clave");
	String puerto = properties.getProperty("SBP_puerto");
	String TxFromCredit = properties.getProperty("SBP_TxFromCredit");
	String codigoInst = properties.getProperty("codigoInst");
	String SBP_VDN_MP_OPC_0 = properties.getProperty("SBP_VDN_MP_OPC_0");
	String SBP_VDN_MP_OPC_1 = properties.getProperty("SBP_VDN_MP_OPC_1");
	String SBP_VDN_MP_OPC_1_1 = properties.getProperty("SBP_VDN_MP_OPC_1_1");
	String SBP_VDN_MP_OPC_1_2 = properties.getProperty("SBP_VDN_MP_OPC_1_2");
	String SBP_VDN_MP_OPC_1_3 = properties.getProperty("SBP_VDN_MP_OPC_1_3");
	String SBP_VDN_MP_OPC_1_4 = properties.getProperty("SBP_VDN_MP_OPC_1_4");
	String SBP_VDN_MP_OPC_2 = properties.getProperty("SBP_VDN_MP_OPC_2");
	String SBP_VDN_MP_OPC_3 = properties.getProperty("SBP_VDN_MP_OPC_3");
	String SBP_VDN_MP_OPC_3_1 = properties.getProperty("SBP_VDN_MP_OPC_3_1");
	String SBP_VDN_MP_OPC_3_2 = properties.getProperty("SBP_VDN_MP_OPC_3_2");
	String SBP_VDN_MP_OPC_3_1_Pref = properties.getProperty("SBP_VDN_MP_OPC_3_1_Pref");
	String SBP_VDN_MP_OPC_3_2_Pref = properties.getProperty("SBP_VDN_MP_OPC_3_2_Pref");
	String SBP_VDN_MP_OPC_4 = properties.getProperty("SBP_VDN_MP_OPC_4");
	String SBP_VDN_MP_OPC_5 = properties.getProperty("SBP_VDN_MP_OPC_5");	
	String SBP_VDN_MP_OPC_7 = properties.getProperty("SBP_VDN_MP_OPC_7");
	String SBP_VDN_MP_OPC_8 = properties.getProperty("SBP_VDN_MP_OPC_8");
	String SBP_VDN_MP_OPC_M = properties.getProperty("SBP_VDN_MP_OPC_M");
	String SBP_VDN_MP_OPC_NV = properties.getProperty("SBP_VDN_MP_OPC_NV");
	String SBP_VDN_MP_OPC_TO = properties.getProperty("SBP_VDN_MP_OPC_TO");

	String broadcast_MP = properties.getProperty("SBP_Broadcast_MP");
	String broadcast_OPC0 = properties.getProperty("SBP_Broadcast_Opc0");
	String broadcast_OPC1 = properties.getProperty("SBP_Broadcast_Opc1");
	String broadcast_OPC2 = properties.getProperty("SBP_Broadcast_Opc2");
	String broadcast_OPC3 = properties.getProperty("SBP_Broadcast_Opc3");
	String broadcast_OPC4 = properties.getProperty("SBP_Broadcast_Opc4");
	String broadcast_OPCM = properties.getProperty("SBP_Broadcast_OpcM");
	
	String SBP_SubSeg_1_ini = properties.getProperty("SBP_SubSeg_1_ini");
	String SBP_SubSeg_2_ini = properties.getProperty("SBP_SubSeg_2_ini");
	String SBP_SubSeg_3_ini = properties.getProperty("SBP_SubSeg_3_ini");
	String SBP_SubSeg_4_ini = properties.getProperty("SBP_SubSeg_4_ini");
	String SBP_SubSeg_1_fin = properties.getProperty("SBP_SubSeg_1_fin");
	String SBP_SubSeg_2_fin = properties.getProperty("SBP_SubSeg_2_fin");
	String SBP_SubSeg_3_fin = properties.getProperty("SBP_SubSeg_3_fin");
	String SBP_SubSeg_4_fin = properties.getProperty("SBP_SubSeg_4_fin");
	
	String SBP_Prioridad_VDN_1 = properties.getProperty("SBP_Prioridad_VDN_1");
	String SBP_Prioridad_VDN_2 = properties.getProperty("SBP_Prioridad_VDN_2");
	String SBP_Prioridad_VDN_3 = properties.getProperty("SBP_Prioridad_VDN_3");
	String SBP_Prioridad_VDN_4 = properties.getProperty("SBP_Prioridad_VDN_4");
	
	String SBP_NO_CLIENTE_VDN = properties.getProperty("SBP_NO_CLIENTE_VDN");
	
	String SBP_Premium = properties.getProperty("SBP_PREMIUM");
	
	String SBP_Seg_Preferente_VDN = properties.getProperty("SBP_SEG_PREFERENTE_VDN");
	String SBP_Seg_Personal_VDN = properties.getProperty("SBP_SEG_PERSONAL_VDN");
	String SBP_Seg_Estandar_VDN = properties.getProperty("SBP_SEG_ESTANDAR_VDN");
	String SBP_Seg_Otros_VDN = properties.getProperty("SBP_SEG_OTROS_VDN");
	
	
	String SBP_Lealtad_Todos_VDN = properties.getProperty("SBP_LEALTAD_TODOS_VDN");
	String SBP_Lealtad_Alto_VDN = properties.getProperty("SBP_LEALTAD_ALTO_VDN");
	String SBP_Lealtad_Medio_VDN = properties.getProperty("SBP_LEALTAD_MEDIO_VDN");
	String SBP_Lealtad_Bajo_VDN = properties.getProperty("SBP_LEALTAD_BAJO_VDN");
	String SBP_Lealtad_Nulo_VDN = properties.getProperty("SBP_LEALTAD_NULO_VDN");
	String SBP_Lealtad_Otros_VDN = properties.getProperty("SBP_LEALTAD_OTROS_VDN");
	String SBP_Prioridad_VDN_SEG_30 = properties.getProperty("SBP_Prioridad_VDN_SEG_30"); //Codigo para cliente Beyond segmento 30
    String ACTIVAR_BEYOND_SEG_30 = properties.getProperty("ACTIVAR_BEYOND_SEG_30"); //Activa flujo beyond segmento 30
	
	String SBP_VDN_MP_OPC_4_SEGURO_CARDIF = properties.getProperty("SBP_VDN_MP_OPC_4_SEGURO_CARDIF");
	String SBP_VDN_MP_OPC_4_SEGURO_POSITIVA = properties.getProperty("SBP_VDN_MP_OPC_4_SEGURO_POSITIVA");
	String SBP_VDN_MP_OPC_4_SEGURO_OTRAS_CONSULTAS = properties.getProperty("SBP_VDN_MP_OPC_4_SEGURO_OTRAS_CONSULTAS");
	
	
	setLog(strCallUUID + " IBCC-SBP :::: Servidor WAS: " + servidorwas);
	setLog(strCallUUID + " IBCC-SBP :::: Transferencia Agente: " + TrxAgente);
	setLog(strCallUUID + " IBCC-SBP :::: Transmision Activa Menu Principal: " + TransActivaMP);
	setLog(strCallUUID + " IBCC-SBP :::: Transmision Activa Menu Principal - Opcion 0: " + TransActivaMP_Opc0);
	setLog(strCallUUID + " IBCC-SBP :::: Transmision Activa Menu Principal - Opcion 1: " + TransActivaMP_Opc1);
	setLog(strCallUUID + " IBCC-SBP :::: Transmision Activa Menu Principal - Opcion 2: " + TransActivaMP_Opc2);
	setLog(strCallUUID + " IBCC-SBP :::: Transmision Activa Menu Principal - Opcion 3: " + TransActivaMP_Opc3);
	setLog(strCallUUID + " IBCC-SBP :::: Transmision Activa Menu Principal - Opcion 4: " + TransActivaMP_Opc4);
	setLog(strCallUUID + " IBCC-SBP :::: Transmision Activa Menu Principal - Opcion #: " + TransActivaMP_OpcM);
	setLog(strCallUUID + " IBCC-SBP :::: Servidor Base de Datos: " + servidor_bd);
	setLog(strCallUUID + " IBCC-SBP :::: Usuario: " + usuario);
	setLog(strCallUUID + " IBCC-SBP :::: Clave: " + clave);
	setLog(strCallUUID + " IBCC-SBP :::: Puerto: " + puerto);
	setLog(strCallUUID + " IBCC-SBP :::: Transferencia de Credito Activa?: " + TxFromCredit);	
	setLog(strCallUUID + " IBCC-SBP :::: Codigo de Institucion: " + codigoInst);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 0: " + SBP_VDN_MP_OPC_0);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 1: " + SBP_VDN_MP_OPC_1);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 1_1: " + SBP_VDN_MP_OPC_1_1);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 1_2: " + SBP_VDN_MP_OPC_1_2);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 1_3: " + SBP_VDN_MP_OPC_1_3);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 1_4: " + SBP_VDN_MP_OPC_1_4);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 2: " + SBP_VDN_MP_OPC_2);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 3: " + SBP_VDN_MP_OPC_3);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 3_1: " + SBP_VDN_MP_OPC_3_1);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 3_2: " + SBP_VDN_MP_OPC_3_2);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 3_1_Pref: " + SBP_VDN_MP_OPC_3_1_Pref);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 3_2_Pref: " + SBP_VDN_MP_OPC_3_2_Pref);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 4: " + SBP_VDN_MP_OPC_4);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 5: " + SBP_VDN_MP_OPC_5);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 7: " + SBP_VDN_MP_OPC_7);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion 8: " + SBP_VDN_MP_OPC_8);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion #: " + SBP_VDN_MP_OPC_M);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion Opc. No Valido: " + SBP_VDN_MP_OPC_NV);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Menu Principal Opcion TimeOut: " + SBP_VDN_MP_OPC_TO);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 1 inicio: " + SBP_SubSeg_1_ini);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 2 inicio: " + SBP_SubSeg_2_ini);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 3 inicio: " + SBP_SubSeg_3_ini);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 4 inicio: " + SBP_SubSeg_4_ini);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 1 fin: " + SBP_SubSeg_1_fin);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 2 fin: " + SBP_SubSeg_2_fin);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 3 fin: " + SBP_SubSeg_3_fin);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Sub Segmento 4 fin: " + SBP_SubSeg_4_fin);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Prioridad de atencion 1: " + SBP_Prioridad_VDN_1);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Prioridad de atencion 2: " + SBP_Prioridad_VDN_2);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Prioridad de atencion 3: " + SBP_Prioridad_VDN_3);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Prioridad de atencion 4: " + SBP_Prioridad_VDN_4);
	setLog(strCallUUID + " IBCC-SBP :::: VDN Prioridad de No cliente: " + SBP_NO_CLIENTE_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_PREMIUM: " + SBP_Premium);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_PREFERENTE_VDN: " + SBP_Seg_Preferente_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_PERSONAL_VDN: " + SBP_Seg_Personal_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_SEG_ESTANDAR_VDN: " + SBP_Seg_Estandar_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_LEALTAD_TODOS_VDN: " + SBP_Lealtad_Todos_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_LEALTAD_ALTO_VDN: " + SBP_Lealtad_Alto_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_LEALTAD_MEDIO_VDN: " + SBP_Lealtad_Medio_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_LEALTAD_BAJO_VDN: " + SBP_Lealtad_Bajo_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_LEALTAD_NULO_VDN: " + SBP_Lealtad_Nulo_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_LEALTAD_OTROS_VDN: " + SBP_Lealtad_Otros_VDN);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_Prioridad_VDN_SEG_30: " + SBP_Prioridad_VDN_SEG_30);
	setLog(strCallUUID + " IBCC-SBP :::: ACTIVAR_BEYOND_SEG_30: " + ACTIVAR_BEYOND_SEG_30);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_VDN_MP_OPC_4_SEGURO_CARDIF: " + SBP_VDN_MP_OPC_4_SEGURO_CARDIF);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_VDN_MP_OPC_4_SEGURO_POSITIVA: " + SBP_VDN_MP_OPC_4_SEGURO_POSITIVA);
	setLog(strCallUUID + " IBCC-SBP :::: SBP_VDN_MP_OPC_4_SEGURO_OTRAS_CONSULTAS: " + SBP_VDN_MP_OPC_4_SEGURO_OTRAS_CONSULTAS);
	
	
	// INICIO - Desencriptar CLAVE BD
		try{
			Encripta encripta = new Encripta();
			String claveDesencriptada = encripta.decrypt(clave);
			String usuarioDesencriptado = encripta.decrypt(usuario);
			clave = claveDesencriptada;
			usuario = usuarioDesencriptado;
			
		}catch (Exception e) {
			setLog(strCallUUID + " IBCC-SBP :::: Error al desencriptar Clave BD: " + e);
		}	
	// FIN - Desencriptar CLAVE BD
	
	String SBP_Broadcast_MP = "", SBP_Broadcast_Opc0 = "", SBP_Broadcast_Opc1 = "", SBP_Broadcast_Opc2 = "", SBP_Broadcast_Opc3 = "", SBP_Broadcast_Opc4 = "", SBP_Broadcast_OpcM = "";
	String extension=".wav";
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruSBP/AudiosIBCC";
	
	if(broadcast_MP.equals("")){
		SBP_Broadcast_MP = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_MP = url_audio + "/" + broadcast_MP + extension;
	}
	
	if(broadcast_OPC0.equals("")){
		SBP_Broadcast_Opc0 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc0 = url_audio + "/" + broadcast_OPC0 + extension;
	}
	
	if(broadcast_OPC1.equals("")){
		SBP_Broadcast_Opc1 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc1 = url_audio + "/" + broadcast_OPC1 + extension;
	}
	
	if(broadcast_OPC2.equals("")){
		SBP_Broadcast_Opc2 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc2 = url_audio + "/" + broadcast_OPC2 + extension;
	}
	
	if(broadcast_OPC3.equals("")){
		SBP_Broadcast_Opc3 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc3 = url_audio + "/" + broadcast_OPC3 + extension;
	}
	
	if(broadcast_OPC4.equals("")){
		SBP_Broadcast_Opc4 = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_Opc4 = url_audio + "/" + broadcast_OPC4 + extension;
	}
	
	if(broadcast_OPCM.equals("")){
		SBP_Broadcast_OpcM = url_audio + "/silencio" + extension;
	} else {
		SBP_Broadcast_OpcM = url_audio + "/" + broadcast_OPCM + extension;
	}
	
	setLog(strCallUUID + " IBCC-SBP :::: Broadcast del Menu Principal: " + SBP_Broadcast_MP);
	setLog(strCallUUID + " IBCC-SBP :::: Broadcast del Menu Principal Opcion 0: " + SBP_Broadcast_Opc0);
	setLog(strCallUUID + " IBCC-SBP :::: Broadcast del Menu Principal Opcion 1: " + SBP_Broadcast_Opc1);
	setLog(strCallUUID + " IBCC-SBP :::: Broadcast del Menu Principal Opcion 2: " + SBP_Broadcast_Opc2);
	setLog(strCallUUID + " IBCC-SBP :::: Broadcast del Menu Principal Opcion 3: " + SBP_Broadcast_Opc3);
	setLog(strCallUUID + " IBCC-SBP :::: Broadcast del Menu Principal Opcion 4: " + SBP_Broadcast_Opc4);
	setLog(strCallUUID + " IBCC-SBP :::: Broadcast del Menu Principal Opcion M: " + SBP_Broadcast_OpcM);
	
	///-- FIN - Leer Archivo de Configuracion
	
	///-- INICIO - Locucion de Bienvenida.
	String horario = "";         
    
	Calendar calendario = Calendar.getInstance();
	int hour = calendario.get(Calendar.HOUR_OF_DAY);
	int minute = calendario.get(Calendar.MINUTE);
   	int second = calendario.get(Calendar.SECOND);
	int nro_dia = calendario.get(Calendar.DAY_OF_WEEK);
	String dia = String.valueOf(nro_dia);
	
	setLog(strCallUUID + " IBCC-SBP :::: El dia de hoy es : " + dia);
	
   	String shour = Integer.toString(hour);
   	int lhour = shour.length();
   	String sminute = Integer.toString(minute);
   	int lminute = sminute.length();
   	String ssecond = Integer.toString(second);
   	int lsecond = ssecond.length();
   	
   	if(lhour == 1){
   		shour = "0" + shour;}
   	if(lminute == 1){
   		sminute = "0" + sminute;}
   	if(lsecond == 1){
   		ssecond = "0" + ssecond;}
   	
   	String horaActual = shour + sminute + ssecond;
   	setLog(strCallUUID + " La hora actual es : " + horaActual);
   	
   	int ihoraACtual = Integer.parseInt(horaActual);
   	
   	if (ihoraACtual >= 000000 && ihoraACtual < 120000)
   	{
   		//Buenos dias
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruSBP/AudiosIBCC/welcome_1.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}
   	if (ihoraACtual >= 120000 && ihoraACtual < 180000)
   	{
   		//Buenos Tardes  
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruSBP/AudiosIBCC/welcome_2.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}
   	if (ihoraACtual >= 180000 && ihoraACtual < 235959)
   	{
   		//Buenos Noches
   		horario = "http://" + servidorwas + ":8080" + "/APP_IVR_PeruSBP/AudiosIBCC/welcome_3.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + horario);
   	}     	 
	
	///-- FIN - Locucion de Bienvenida.
	
    JSONObject result = new JSONObject();    
    
    
    result.put("servidorwas", servidorwas);
    result.put("TrxAgente", TrxAgente);
    result.put("TransActivaMP", TransActivaMP);
    result.put("TransActivaMP_Opc0", TransActivaMP_Opc0);
    result.put("TransActivaMP_Opc1", TransActivaMP_Opc1);
    result.put("TransActivaMP_Opc2", TransActivaMP_Opc2);
    result.put("TransActivaMP_Opc3", TransActivaMP_Opc3);
    result.put("TransActivaMP_Opc4", TransActivaMP_Opc4);
    result.put("TransActivaMP_OpcM", TransActivaMP_OpcM);
    result.put("servidor_bd", servidor_bd);
    result.put("usuario", usuario);
    result.put("clave", clave);
    result.put("puerto", puerto);
    result.put("TxFromCredit", TxFromCredit);
    result.put("codigoInst", codigoInst);
    result.put("horario", horario);
    result.put("dia", dia);
    result.put("horaActual", horaActual);
    result.put("SBP_VDN_MP_OPC_0", SBP_VDN_MP_OPC_0);
    result.put("SBP_VDN_MP_OPC_1", SBP_VDN_MP_OPC_1);
    result.put("SBP_VDN_MP_OPC_1_1", SBP_VDN_MP_OPC_1_1);
    result.put("SBP_VDN_MP_OPC_1_2", SBP_VDN_MP_OPC_1_2);
    result.put("SBP_VDN_MP_OPC_1_3", SBP_VDN_MP_OPC_1_3);
    result.put("SBP_VDN_MP_OPC_1_4", SBP_VDN_MP_OPC_1_4);
    result.put("SBP_VDN_MP_OPC_2", SBP_VDN_MP_OPC_2);
    result.put("SBP_VDN_MP_OPC_3", SBP_VDN_MP_OPC_3);
    result.put("SBP_VDN_MP_OPC_3_1", SBP_VDN_MP_OPC_3_1);
    result.put("SBP_VDN_MP_OPC_3_2", SBP_VDN_MP_OPC_3_2);
    result.put("SBP_VDN_MP_OPC_3_1_Pref", SBP_VDN_MP_OPC_3_1_Pref);
    result.put("SBP_VDN_MP_OPC_3_2_Pref", SBP_VDN_MP_OPC_3_2_Pref);
    result.put("SBP_VDN_MP_OPC_4", SBP_VDN_MP_OPC_4);
    result.put("SBP_VDN_MP_OPC_5", SBP_VDN_MP_OPC_5);
    result.put("SBP_VDN_MP_OPC_7", SBP_VDN_MP_OPC_7);
    result.put("SBP_VDN_MP_OPC_8", SBP_VDN_MP_OPC_8);
    result.put("SBP_VDN_MP_OPC_M", SBP_VDN_MP_OPC_M);
    result.put("SBP_VDN_MP_OPC_NV", SBP_VDN_MP_OPC_NV);
    result.put("SBP_VDN_MP_OPC_TO", SBP_VDN_MP_OPC_TO);
    result.put("SBP_SubSeg_1_ini", SBP_SubSeg_1_ini);
    result.put("SBP_SubSeg_2_ini", SBP_SubSeg_2_ini);
    result.put("SBP_SubSeg_3_ini", SBP_SubSeg_3_ini);
    result.put("SBP_SubSeg_4_ini", SBP_SubSeg_4_ini);
    result.put("SBP_SubSeg_1_fin", SBP_SubSeg_1_fin);
    result.put("SBP_SubSeg_2_fin", SBP_SubSeg_2_fin);
    result.put("SBP_SubSeg_3_fin", SBP_SubSeg_3_fin);
    result.put("SBP_SubSeg_4_fin", SBP_SubSeg_4_fin);
    result.put("SBP_Broadcast_MP", SBP_Broadcast_MP);
    result.put("SBP_Broadcast_Opc0", SBP_Broadcast_Opc0);
    result.put("SBP_Broadcast_Opc1", SBP_Broadcast_Opc1);
    result.put("SBP_Broadcast_Opc2", SBP_Broadcast_Opc2);
    result.put("SBP_Broadcast_Opc3", SBP_Broadcast_Opc3);
    result.put("SBP_Broadcast_Opc4", SBP_Broadcast_Opc4);
    result.put("SBP_Broadcast_OpcM", SBP_Broadcast_OpcM);
    result.put("SBP_Prioridad_VDN_1", SBP_Prioridad_VDN_1);
    result.put("SBP_Prioridad_VDN_2", SBP_Prioridad_VDN_2);
    result.put("SBP_Prioridad_VDN_3", SBP_Prioridad_VDN_3);
    result.put("SBP_Prioridad_VDN_4", SBP_Prioridad_VDN_4);
    result.put("SBP_NO_CLIENTE_VDN", SBP_NO_CLIENTE_VDN);
    result.put("SBP_PREMIUM", SBP_Premium);
    result.put("SBP_VDN_Cabinas", SBP_VDN_Cabinas);
    result.put("SBP_VDN_Stardar_3116040", SBP_VDN_Stardar_3116040);
    result.put("SBP_VDN_Stardar_65533", SBP_VDN_Stardar_65533);
    result.put("SBP_Seg_Preferente_VDN", SBP_Seg_Preferente_VDN);
    result.put("SBP_Seg_Personal_VDN", SBP_Seg_Personal_VDN);
    result.put("SBP_Seg_Estandar_VDN", SBP_Seg_Estandar_VDN);
    result.put("SBP_Seg_Otros_VDN", SBP_Seg_Otros_VDN);
    result.put("SBP_Lealtad_Todos_VDN", SBP_Lealtad_Todos_VDN);
    result.put("SBP_Lealtad_Alto_VDN", SBP_Lealtad_Alto_VDN);
    result.put("SBP_Lealtad_Medio_VDN", SBP_Lealtad_Medio_VDN);
    result.put("SBP_Lealtad_Bajo_VDN", SBP_Lealtad_Bajo_VDN);
    result.put("SBP_Lealtad_Nulo_VDN", SBP_Lealtad_Nulo_VDN);
    result.put("SBP_Lealtad_Otros_VDN", SBP_Lealtad_Otros_VDN);
	result.put("SBP_Prioridad_VDN_SEG_30", SBP_Prioridad_VDN_SEG_30);
	result.put("ACTIVAR_BEYOND_SEG_30", ACTIVAR_BEYOND_SEG_30);
	result.put("SBP_VDN_MP_OPC_4_SEGURO_CARDIF", SBP_VDN_MP_OPC_4_SEGURO_CARDIF);
	result.put("SBP_VDN_MP_OPC_4_SEGURO_POSITIVA", SBP_VDN_MP_OPC_4_SEGURO_POSITIVA);
	result.put("SBP_VDN_MP_OPC_4_SEGURO_OTRAS_CONSULTAS", SBP_VDN_MP_OPC_4_SEGURO_OTRAS_CONSULTAS);
    
        
    return result;    
};

public String consultarIp() throws UnknownHostException {
	InetAddress address = InetAddress.getLocalHost();
	//String sHostName = address.getHostName();
	 byte[] bIPAddress = address.getAddress();
	 String sIPAddress = "";
		 for (int x=0; x< bIPAddress.length; x++) {
			 if (x > 0) {
				 sIPAddress += ".";
			 }
		 sIPAddress += bIPAddress[x] & 255;
		 }
	return "//"+sIPAddress;
}
	
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>