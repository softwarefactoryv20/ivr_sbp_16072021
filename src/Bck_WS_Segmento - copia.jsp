<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    String TOKEN = additionalParams.get("TOKEN");
    String documento = additionalParams.get("documento");
    String tipo = "1";
    String ServidorBus = additionalParams.get("ServidorBus");
    String PuertoBus = additionalParams.get("PuertoBus");
    String strCallUUID = state.getString("CallUUID");
    setLog(strCallUUID + " IBCC-SBP :::: Authorization = " + TOKEN);
    setLog(strCallUUID + " IBCC-SBP :::: DNI = " + documento);
    
    JSONObject result = new JSONObject();

		String url = "https://"+ServidorBus+":"+PuertoBus+"/gssbp-customer/profile/document/v1";
		String Body = "{\"documentNumber\": \""+documento+"\", \"documentType\": \""+tipo+"\"}";
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Body = " + Body);
    
    
TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        
        // Install the all-trusting trust manager
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("charset", "utf-8");
		con.setRequestProperty("Authorization", TOKEN);
		byte[] input = Body.getBytes();
		//String bodylen = new String(input.length());
		con.setRequestProperty("Content-Length", "80");
    	
    	OutputStream os = con.getOutputStream();
    	byte[] input2 = Body.getBytes("utf-8");
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		String tempString = response.toString();
		String segment = "NULL";
		

		try{
			JSONObject jsonobj = new JSONObject(tempString);
			segment = jsonobj.getString("segment");							 
		}catch(JSONException Error){
			setLog(strCallUUID + " IBCC-SBP :::: Response Error Json = " + Error);
		}

   		//print result
		setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + tempString);	
		setLog(strCallUUID + " IBCC-SBP :::: Response segment  = " + segment);	
		
    result.put("Segmento", segment);    
    return result;
    
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>
<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>