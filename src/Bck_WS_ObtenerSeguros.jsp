<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String ServidorBus = additionalParams.get("ServidorBus");
    String PuertoBus = additionalParams.get("PuertoBus");  
    String numeroDocumento = additionalParams.get("numeroDocumento");
    String tipoDocumento = additionalParams.get("tipoDocumento");
    String token = additionalParams.get("token");  
	
    String tipoDoc = "";
    
    if(tipoDocumento.equals("DNI"))
    	tipoDoc = "1";
    else
    	tipoDoc = "2";
    
    String tempString = "";
	JSONObject jsonRespuesta = new JSONObject();
	
    JSONObject result = new JSONObject();
    String strCallUUID = state.getString("CallUUID");
    
    try{
		String url = "https://"+ServidorBus+":"+PuertoBus+"/gssbp-customer-insurance/search/v1";
		String Body = "{\"documentNumber\": \""+numeroDocumento+"\",\"documentType\": \""+tipoDoc+"\"}";
		
	    setLog(strCallUUID + " IBCC-SBP :::: BCK_WS_OBTENER_SEGUROS" );	
	    
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Body = " + Body);
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Authorization", token);
		byte[] input = Body.getBytes();
    	
    	OutputStream os = con.getOutputStream();
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
    	
   		//print result
		tempString = response.toString();
		setLog(strCallUUID + " IBCC-SBP :::: 'POST' response Body = " + tempString);

		try{
			jsonRespuesta = new JSONObject(tempString);
		}catch(JSONException Error){
			setLog(strCallUUID + " IBCC-SBP :::: Response Error Json = " + Error);
		}
    }catch(Exception Error){
		setLog(strCallUUID + " IBCC-SBP :::: Response Error = " + Error);
	}

	result.put("seguros", jsonRespuesta);
	

    return result;

};
%>
<%-- GENERATED: DO NOT REMOVE --%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>

<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>

<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>

