<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="middleware_support.tramas.Bean_IB92"%>
<%@page import= "IvrTransaction.Controller.GeneralController"%>
<%@page import= "IvrTransaction.modelo.SaldosTC"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	//sesion =  strCallUUID + ", ";
    Logger log = Logger.getLogger("Bck_IB92.jsp");
	// TODO Auto-generated method stub
			String extension=".wav";
			String servidorwas = additionalParams.get("servidorwas");
			String url = "http://" + servidorwas + ":8080";
			String url_audio = url + "/APP_IVR_PeruSBP/AudiosIBCC";
			
			//Input
			String vInstitucion=additionalParams.get("codigoInst");
			String vTarjeta= additionalParams.get("vNumDoc");
			
			//Output
			int vCodErrorRpta = 0;
			String vCodRpta="0000"; 
			String client_desc="";
			String indMultimoneda="";
			String moneda="";
			String deudaTotalFacturadaSoles="";
			String pagoMesSoles="";
			String pagoMinimoSoles="";
			String deudaTotalFacturadaDolares="";
			String pagoMesDolares="";
			String pagoMinimoDolares="";
			
			Bean_IB92 refBean_IB92;
			
			
			JSONObject result = new JSONObject();		    
			
			IvrTransaction.Controller.GeneralController General= new IvrTransaction.Controller.GeneralController();
			IvrTransaction.modelo.SaldosTC generalModel = General.QIvrSaldosTC(vInstitucion, vTarjeta);
			vCodErrorRpta = General.getCodRetorno();
			
		    if (vCodErrorRpta == 0) {  
			
				vCodRpta=generalModel.getERROR();
				if (vCodRpta.equals("0000")) {
					
					client_desc=generalModel.getNombreCliente();
					refBean_IB92=new Bean_IB92(url_audio,generalModel,extension);
					indMultimoneda=refBean_IB92.getIndMultiMoneda();
					moneda=refBean_IB92.getMonedaTarjeta();
					deudaTotalFacturadaSoles=refBean_IB92.getPagoTotalSoles().getNumero();
					pagoMesSoles=refBean_IB92.getPagoMinimitoSoles().getNumero();
					pagoMinimoSoles=refBean_IB92.getPagoMinimoSoles().getNumero();
					deudaTotalFacturadaDolares=refBean_IB92.getPagoTotalDolares().getNumero();
					pagoMesDolares=refBean_IB92.getPagoMinimitoDolares().getNumero();
					pagoMinimoDolares=refBean_IB92.getPagoMinimoDolares().getNumero();
			
					
					
					setLog(strCallUUID + " IBCC-SBP :::: ========= INVOCACION - IB92 : SALDOS TARJETA DE CREDITO ========= ");
					
					setLog(strCallUUID + " IBCC-SBP :::: Cod. Producto: " + refBean_IB92.getNroTarjetaCredito());
					setLog(strCallUUID + " IBCC-SBP :::: Cliente:  " + client_desc);
				
					setLog(strCallUUID + " IBCC-SBP :::: Moneda: " + refBean_IB92.getMonedaTarjeta());
					setLog(strCallUUID + " IBCC-SBP :::: Ind. Multimoneda: " + refBean_IB92.getIndMultiMoneda());
					setLog(strCallUUID + " IBCC-SBP :::: Minimo Soles: " + refBean_IB92.getPagoMinimoSoles().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Total Soles: " + refBean_IB92.getPagoTotalSoles().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Fact. Soles: " + refBean_IB92.getPagoFactSoles().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Minimito Soles: " + refBean_IB92.getPagoMinimitoSoles().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Totalito Soles: " + refBean_IB92.getPagoTotalitoSoles().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Minimo Dolares: " + refBean_IB92.getPagoMinimoDolares().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Total Dolares: " + refBean_IB92.getPagoTotalDolares().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Fact. Dolares: " + refBean_IB92.getPagoFactDolares().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Minimito Dolares: " + refBean_IB92.getPagoMinimitoDolares().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Totalito Dolares: " + refBean_IB92.getPagoTotalitoDolares().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Linea Credito: " + refBean_IB92.getLineaCredito().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Linea Disp. Efectivo: " + refBean_IB92.getLineaDispEfectivo().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Saldo Credito: " + refBean_IB92.getSaldoCredito().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Saldo Linea Disp. Efectivo: " + refBean_IB92.getSaldoLineaDispEfectivo().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Ultimo Pago Mon. Local: " + refBean_IB92.getUltPagoMonLocal().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Ultimo Pago Mon. Ext. : " + refBean_IB92.getUltPagoMonExt().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Fecha Ultimo Pago: " + refBean_IB92.getFecUltPago().getFecha());
					setLog(strCallUUID + " IBCC-SBP :::: Scotia Ptos. Saldo Tarjeta: " + refBean_IB92.getScotiaPtosSalxTarjeta().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Sctoia Ptos. Saldo Total:  " + refBean_IB92.getScotiaPtosSalTotal().getNumero());
					setLog(strCallUUID + " IBCC-SBP :::: Procesadora: " + refBean_IB92.getProcesadora());
					
					setLog(strCallUUID + " IBCC-SBP :::: ------------Output--------------");
						
					setLog(strCallUUID + " IBCC-SBP :::: Indicador Multimoneda :"+indMultimoneda);
					setLog(strCallUUID + " IBCC-SBP :::: Moneda :"+moneda);
					setLog(strCallUUID + " IBCC-SBP :::: Deuda Total Facturada Soles : "+deudaTotalFacturadaSoles);
					setLog(strCallUUID + " IBCC-SBP :::: Pago Mes Soles: "+pagoMesSoles);
					setLog(strCallUUID + " IBCC-SBP :::: Pago Minimo Soles: "+pagoMinimoSoles);
					setLog(strCallUUID + " IBCC-SBP :::: Deuda Total Facturada Dolares : "+deudaTotalFacturadaDolares);
					setLog(strCallUUID + " IBCC-SBP :::: Pago Mes Dolares: "+pagoMesDolares);
					setLog(strCallUUID + " IBCC-SBP :::: Pago Minimo Dolares: "+pagoMinimoDolares);
					
				}		
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  			
			}
		    
			setLog(strCallUUID + " IBCC-SBP :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
			setLog(strCallUUID + " IBCC-SBP :::: Codigo Retorno Transaccion OK = " + vCodRpta);
			
			
			result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
			result.put("vCodRpta", vCodRpta);
			result.put("multimoneda", indMultimoneda);
			result.put("monedaServicio", moneda);
			result.put("pagoDeudaTotalSoles", deudaTotalFacturadaSoles);
			result.put("pagoDeudaMesSoles", pagoMesSoles);
			result.put("pagoDeudaMinimoSoles", pagoMinimoSoles);
			result.put("pagoDeudaTotalDolares", deudaTotalFacturadaDolares);
			result.put("pagoDeudaMesDolares", pagoMesDolares);
			result.put("pagoDeudaMinimoDolares", pagoMinimoDolares);
			
		    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>