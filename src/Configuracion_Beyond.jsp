<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	
	
	///-- INICIO - Leer Archivo de Configuracion
	
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	String nc_DNIS = additionalParams.get("nc_DNIS");

	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Configuracion_Beyond.jsp");
	setLog(strCallUUID + " IBCC-SBP :::: Primera linea de LOG Configuracion_Beyond.jsp");
	//--- INICIO vdn_cabinas.xml
	
	
	String path_Agencia_Remota = null;
	try {
		
		path_Agencia_Remota = consultarIp() + "/compartido/inicio_SBP_BEYOND.xml";
		setLog(strCallUUID + " IBCC-SBP :::: Ruta XML Agencia Remota = " + path_Agencia_Remota);
	} catch (UnknownHostException e1) {
		e1.printStackTrace();
	}
	
	java.io.File file_Agencia_Remota = new java.io.File(path_Agencia_Remota);
	Properties properties_Agencia_Remota = new Properties();
	java.io.InputStream inputStream_cab=null;
	
	if(file_Agencia_Remota.exists()){
		properties_Agencia_Remota.clear();
				
		try {
			inputStream_cab = new java.io.FileInputStream(file_Agencia_Remota);
		    } 
		catch (FileNotFoundException e) {
			e.printStackTrace(); }
		
		try {
			properties_Agencia_Remota.loadFromXML(inputStream_cab);
		    } 
		catch (InvalidPropertiesFormatException e) {
 			 e.printStackTrace(); } 
		catch (IOException e) {
			e.printStackTrace(); }		
	}
	String SBP_SERVIDOR_WAS = properties_Agencia_Remota.getProperty("SBP_servidorWAS");
	String SBP_PUERTO_WAS = properties_Agencia_Remota.getProperty("SBP_PuertoWAS");
	String SBP_SERVIDOR_BUS = properties_Agencia_Remota.getProperty("SBP_servidorBUS");
	String SBP_PUERTO_BUS = properties_Agencia_Remota.getProperty("SBP_PuertoBUS");	
	String SBP_VDN_DEFAULT = properties_Agencia_Remota.getProperty("SBP_VDN_DEFAULT");
	String SBP_VDN_PREMIUM = properties_Agencia_Remota.getProperty("SBP_VDN_PREMIUM");
	String SBP_VDN_ESTANDAR = properties_Agencia_Remota.getProperty("SBP_VDN_ESTANDAR");
	String SBP_VDN_NOCLIENTE = properties_Agencia_Remota.getProperty("SBP_VDN_NOCLIENTE");
	String SBP_VDN_CE = properties_Agencia_Remota.getProperty("SBP_VDN_CE");
	String SBP_CODIGO_INST = properties_Agencia_Remota.getProperty("SBP_CODIGO_INST");
	

	setLog(strCallUUID + " IBCC-SBP :::: Config SBP_SERVIDOR_BUS: " + SBP_SERVIDOR_BUS);
	setLog(strCallUUID + " IBCC-SBP :::: Config SBP_PUERTO_BUS: " + SBP_PUERTO_BUS);
	setLog(strCallUUID + " IBCC-SBP :::: Config DEFAULT: " + SBP_VDN_DEFAULT);
	setLog(strCallUUID + " IBCC-SBP :::: Config PREMIUM: " + SBP_VDN_PREMIUM);
	setLog(strCallUUID + " IBCC-SBP :::: Config ESTANDAR: " + SBP_VDN_ESTANDAR);
	setLog(strCallUUID + " IBCC-SBP :::: Config NO CLIENTE: " + SBP_VDN_NOCLIENTE);
	setLog(strCallUUID + " IBCC-SBP :::: Config Con Carnet de Extranjería: " + SBP_VDN_CE);
	setLog(strCallUUID + " IBCC-SBP :::: Config SBP_CODIGO_INST: " + SBP_CODIGO_INST);
	//--- FIN vdn_Agencia.xml
	
	///-- INICIO - Locucion de Bienvenida.
	String SBP_AUDIO_BIENVENIDA = "http://" + SBP_SERVIDOR_WAS + ":8080" + "/APP_IVR_AGENCIA_REMOTA/Resources/Prompts/en-US/Agencia/welcome_1.wav";         
    
	Calendar calendario = Calendar.getInstance();
	int hour = calendario.get(Calendar.HOUR_OF_DAY);
	int minute = calendario.get(Calendar.MINUTE);
   	int second = calendario.get(Calendar.SECOND);
	int nro_dia = calendario.get(Calendar.DAY_OF_WEEK);
	String dia = String.valueOf(nro_dia);
	
	setLog(strCallUUID + " IBCC-SBP :::: El dia de hoy es : " + dia);
	
   	String shour = Integer.toString(hour);
   	int lhour = shour.length();
   	String sminute = Integer.toString(minute);
   	int lminute = sminute.length();
   	String ssecond = Integer.toString(second);
   	int lsecond = ssecond.length();
   	
   	if(lhour == 1){
   		shour = "0" + shour;}
   	if(lminute == 1){
   		sminute = "0" + sminute;}
   	if(lsecond == 1){
   		ssecond = "0" + ssecond;}
   	
   	String horaActual = shour + sminute + ssecond;
   	setLog(strCallUUID + " La hora actual es : " + horaActual);
   	
   	int ihoraACtual = Integer.parseInt(horaActual);
   	
   	if (ihoraACtual >= 000000 && ihoraACtual < 120000)
   	{
   		//Buenos dias
   		SBP_AUDIO_BIENVENIDA = "http://" + SBP_SERVIDOR_WAS + ":8080" + "/APP_IVR_AGENCIA_REMOTA/Resources/Prompts/en-US/Agencia/welcome_1.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + SBP_AUDIO_BIENVENIDA);
   	}
   	if (ihoraACtual >= 120000 && ihoraACtual < 180000)
   	{
   		//Buenos Tardes  
   		SBP_AUDIO_BIENVENIDA = "http://" + SBP_SERVIDOR_WAS + ":8080" + "/APP_IVR_AGENCIA_REMOTA/Resources/Prompts/en-US/Agencia/welcome_2.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + SBP_AUDIO_BIENVENIDA);
   	}
   	if (ihoraACtual >= 180000 && ihoraACtual < 235959)
   	{
   		//Buenos Noches
   		SBP_AUDIO_BIENVENIDA = "http://" + SBP_SERVIDOR_WAS + ":8080" + "/APP_IVR_AGENCIA_REMOTA/Resources/Prompts/en-US/Agencia/welcome_3.wav";
   		setLog(strCallUUID + " El audio a reproducir es: " + SBP_AUDIO_BIENVENIDA);
   	}     	 
	
	///-- FIN - Locucion de Bienvenida.
	
	
    JSONObject result = new JSONObject();    
    
    result.put("SBP_ServidorBus", SBP_SERVIDOR_BUS);
    result.put("SBP_PuertoBus", SBP_PUERTO_BUS);
    result.put("SBP_VDN_DEFAULT", SBP_VDN_DEFAULT);
    result.put("SBP_VDN_PREMIUM", SBP_VDN_PREMIUM);
    result.put("SBP_VDN_ESTANDAR", SBP_VDN_ESTANDAR);
    result.put("SBP_VDN_NOCLIENTE", SBP_VDN_NOCLIENTE);
    result.put("SBP_AUDIO_BIENVENIDA",SBP_AUDIO_BIENVENIDA);
    result.put("SBP_VDN_CE",SBP_VDN_CE);
    result.put("SBP_CODIGO_INST",SBP_CODIGO_INST);
    
    return result;    
};

public String consultarIp() throws UnknownHostException {
	InetAddress address = InetAddress.getLocalHost();
	//String sHostName = address.getHostName();
	 byte[] bIPAddress = address.getAddress();
	 String sIPAddress = "";
		 for (int x=0; x< bIPAddress.length; x++) {
			 if (x > 0) {
				 sIPAddress += ".";
			 }
		 sIPAddress += bIPAddress[x] & 255;
		 }
	return "//"+sIPAddress;
}
	
%>

<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.net.InetAddress"%>
<%@page import="java.net.UnknownHostException"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.InvalidPropertiesFormatException"%>
<%@page import="java.util.Properties"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>