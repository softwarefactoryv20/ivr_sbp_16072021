<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	Logger log = Logger.getLogger("http_Request_Set_EDU.jsp");
	setLog(strCallUUID + " IBCC-SBP :::: Primera linea de LOG http_Request_Set_EDU_PREMIUM.jsp");
	
	String vmain =  "http://SPCC-WTS2.per.bns:9171/bootcmp.htm?action=start_script&script=httpvox.iccmd&command=setedu&";
	String USER_AGENT = "Mozilla/5.0";
	//String v_Start_Time = additionalParams.get("v_Start_Time");
	String vedu = additionalParams.get("eduid");
	String vapp_prd = "BRU";		
	String ivr_num = "Premium Menu Principal";
	String vaut_type = additionalParams.get("aut_type");
	String vprimary_ani = additionalParams.get("nc_ANI");
	String vprimary_dnis = additionalParams.get("nc_DNIS");
	String vani = additionalParams.get("nc_ANI");
	String vdnis = additionalParams.get("nc_DNIS");
	String vclient_desc = additionalParams.get("client_desc");
	String vid_num = additionalParams.get("id_num");
	String vbru_name = additionalParams.get("flagClientCampana");
	String vbru_addr = additionalParams.get("flagTarjetaBloqueada");
	String vbru_phone = additionalParams.get("ctaBT");
	String vCumpleanio = additionalParams.get("cif_key");
	String cif_key = vbru_phone;
	String city_desc = additionalParams.get("city_desc");
	String ctry_desc = additionalParams.get("ctry_desc");
	
	//setLog(strCallUUID + " IBCC-SBP :::: v_Start_Time = " + v_Start_Time);
	setLog(strCallUUID + " IBCC-SBP :::: vedu = " + vedu);	
	setLog(strCallUUID + " IBCC-SBP :::: vapp_prd = " + vapp_prd);					
	setLog(strCallUUID + " IBCC-SBP :::: ivr_num = " + ivr_num);					
	setLog(strCallUUID + " IBCC-SBP :::: vaut_type = " + vaut_type);					
	setLog(strCallUUID + " IBCC-SBP :::: vprimary_ani = " + vprimary_ani);			
	setLog(strCallUUID + " IBCC-SBP :::: vprimary_dnis = " + vprimary_dnis);				
	setLog(strCallUUID + " IBCC-SBP :::: vani = " + vani);
	setLog(strCallUUID + " IBCC-SBP :::: vdnis = " + vdnis);						
	setLog(strCallUUID + " IBCC-SBP :::: vclient_desc = " + vclient_desc);					
	setLog(strCallUUID + " IBCC-SBP :::: vid_num = " + vid_num);					
	setLog(strCallUUID + " IBCC-SBP :::: vbru_name = " + vbru_name);					
	setLog(strCallUUID + " IBCC-SBP :::: vbru_addr = " + vbru_addr);
	setLog(strCallUUID + " IBCC-SBP :::: vbru_phone = " + vbru_phone);
	setLog(strCallUUID + " IBCC-SBP :::: vCumpleanio = " + vCumpleanio);
	setLog(strCallUUID + " IBCC-SBP :::: cif_key = " + cif_key);
	setLog(strCallUUID + " IBCC-SBP :::: city_desc = " + city_desc);
	setLog(strCallUUID + " IBCC-SBP :::: ctry_desc = " + ctry_desc);
	
    JSONObject result = new JSONObject();
    
    /// INICIO - OBTENIENDO LOS SEGUNDOS DESDE QUE EL CLIENTE MARCO LA OPCION 0.
    
    try
    {
    	/*String f_h[] = v_Start_Time.split(",");
    	java.util.Date fechaMenor = new java.util.Date(Integer.parseInt(f_h[0]), Integer.parseInt(f_h[1]), Integer.parseInt(f_h[2]), Integer.parseInt(f_h[3]), Integer.parseInt(f_h[4]), Integer.parseInt(f_h[5]));
    	Calendar calendario = Calendar.getInstance();
    	int anio, mes, dia, hora, minuto, segundo;
    	anio = calendario.get(Calendar.YEAR);
    	mes = calendario.get(Calendar.MONTH) + 1;
    	dia = calendario.get(Calendar.DAY_OF_MONTH);
    	hora = calendario.get(Calendar.HOUR_OF_DAY);
    	minuto = calendario.get(Calendar.MINUTE);
    	segundo = calendario.get(Calendar.SECOND);
    	setLog(strCallUUID + " IBCC-SBP :::: HORA FIN = " +anio + "/" + mes + "/" + dia + " " + hora + ":" + minuto + ":" + segundo);
    	
    	java.util.Date fechaMayor = new java.util.Date(anio, mes, dia, hora, minuto, segundo);
    	long diferenciaMils = fechaMayor.getTime() - fechaMenor.getTime();
    	long solo_segundos = diferenciaMils / 1000;
    	
    	setLog(strCallUUID + " IBCC-SBP :::: Segundos = " + solo_segundos);*/
    	
    	/// FIN
    	
    	/// INICIO - ENVIANDO REQUEST SET_EDU
		
    	String EDUSTR = vmain + "eduid=" + vedu + "&app_prd=" + vapp_prd + "&id_num=" + vid_num + "&aut_type=" + vaut_type +"&primary_ani=" + vprimary_ani + "&primary_dnis=" + vprimary_dnis + "&ani_num=" + vprimary_ani + "&dnis=" + vprimary_dnis + "&transit_num=na&client_desc="+ vclient_desc +"&city_desc=" + city_desc + "&ctry_code=na&ctry_desc=" + ctry_desc + "&scqueuet=na&dnis_num="+ vprimary_dnis +"&ivr_num=" + ivr_num + "&bru_name="+ vbru_name +"&bru_addr="+ vbru_addr +"&bru_phone="+ vbru_phone +"&bru_fax=na&vip="+ vCumpleanio +"&cif_key="+ cif_key + "&inst_id=01";
    	//String EDUSTR = vmain + "eduid=" + vedu + "&app_prd=" + vapp_prd + "&id_num=" + vid_num + "&ani=" + vprimary_ani + "&aut_type=" + vaut_type +"&primary_ani=999999999&primary_dnis=" + vprimary_dnis + "&ani_num=999999999&dnis=" + vprimary_dnis + "&transit_num=na&client_desc="+ vclient_desc +"&city_desc=" + city_desc + "&ctry_code=na&ctry_desc=" + ctry_desc + "&scqueuet=na&dnis_num="+ vprimary_dnis +"&ivr_num=" + ivr_num + "&bru_name="+ vbru_name +"&bru_addr="+ vbru_addr +"&bru_phone="+ vbru_phone +"&bru_fax=na&vip="+ vCumpleanio +"&cif_key=na&inst_id=01";
		
		EDUSTR = EDUSTR.replace(" ", "%20");
		EDUSTR = Normalizer.normalize(EDUSTR, Normalizer.Form.NFD);
		EDUSTR = EDUSTR.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		
    	URL obj = new URL(EDUSTR);
    	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
    	
    	// optional default is GET
    	con.setRequestMethod("GET");

    	//add request header
    	con.setRequestProperty("User-Agent", USER_AGENT);

    	int responseCode = con.getResponseCode();
    	setLog(strCallUUID + " IBCC-SBP :::: Sending 'GET' request to URL = " + EDUSTR);
    	setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + responseCode);

    	BufferedReader in = new BufferedReader(
    	        new InputStreamReader(con.getInputStream()));
    	String inputLine;
    	StringBuffer response = new StringBuffer();

    	while ((inputLine = in.readLine()) != null) {
    		response.append(inputLine);
    	}
    	in.close();

    	//print result
    	setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + response.toString());	
    }
    catch(Exception ex)
	{
    	setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + ex.toString());
	}    
       
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.text.Normalizer"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>