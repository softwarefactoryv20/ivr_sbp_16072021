<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.CallableStatement"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	//INPUT
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	String conexion = additionalParams.get("conexion");
	String trama = additionalParams.get("trama");
	
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bk_UpdateRegistroDNI.jsp");
	
	String conexionArray[] = conexion.split(",");
		
	String vServidor_bd = conexionArray[0];
	String vUsuario = conexionArray[1];
	String vClave = conexionArray[2];
	String vPuerto = conexionArray[3];
	
	setLog(strCallUUID + " IBCC-SBP :::: Servidor: " + vServidor_bd);
	setLog(strCallUUID + " IBCC-SBP :::: Puerto: " + vPuerto);
	
	String tramaArray[] = trama.split(",");
	
	String vCampoE = tramaArray[0];
	String vEtiqueta = tramaArray[1];
	String vCampoO = "";
	String vOpcion = "";
	String vIvr = tramaArray[2];
	String vTipo = tramaArray[3];
	String vOopcionINI = "";
		
	setLog(strCallUUID + " IBCC-SBP :::: Campo Etiqueta: " + vCampoE);
	setLog(strCallUUID + " IBCC-SBP :::: Valor Etiqueta: " + vEtiqueta);
	setLog(strCallUUID + " IBCC-SBP :::: Campo Opcion: " + vCampoO);
	setLog(strCallUUID + " IBCC-SBP :::: Valor Opcion: " + vOpcion);
	setLog(strCallUUID + " IBCC-SBP :::: Opcion Menu Principal: " + vOopcionINI);
	setLog(strCallUUID + " IBCC-SBP :::: Tipo IVR: " + vIvr);
	setLog(strCallUUID + " IBCC-SBP :::: Tipo Cliente: " + vTipo);
	
	//OUTPUT
	String retorno = "0";
	
	JSONObject result = new JSONObject();
		
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	CallableStatement proc = null;
	
	setLog(strCallUUID + " IBCC-SBP :::: =============== BACKEND ACTUALIZAR REGISTRO DE REPORTE DE TRAZABILIDAD - DNI ================================");
	
    try 
    {
    	setLog(strCallUUID + " IBCC-SBP :::: Iniciando Conexion SQL");
    	proc = getMSSQLSERVER(strCallUUID, vServidor_bd, vPuerto, vUsuario, vClave).prepareCall("{ call international_data_store.dbo.sp_update_trazabilidad_llamadas(?,?,?,?,?,?,?,?) }",
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);

      	proc.setString(1, vCampoE);
      	proc.setString(2, vEtiqueta);
      	proc.setString(3, vCampoO);
      	proc.setString(4, vOpcion);
      	proc.setString(5, vOopcionINI);
      	proc.setString(6, vIvr);
      	proc.setString(7, vTipo);
      	proc.setString(8, strCallUUID);
      	
        setLog(strCallUUID + " IBCC-SBP :::: Iniciando ejecucion de SP - sp_update_trazabilidad_llamadas");
        proc.executeUpdate();
        setLog(strCallUUID + " IBCC-SBP :::: Terminando ejecucion de SP - sp_update_trazabilidad_llamadas");
    	setLog(strCallUUID + " IBCC-SBP :::: Codigo de retorno OK: " + retorno);
	} catch (Exception e) {
		setLog(strCallUUID + " IBCC-SBP :::: ERROR - SE EJECUTO INCORRECTAMENTE EL QUERY");
    	e.printStackTrace();
    	retorno = "-1";
    	setLog(strCallUUID + " IBCC-SBP :::: Codigo de retorno ERROR: " + retorno);
    	setLog(strCallUUID + " IBCC-SBP :::: Descripcion del ERROR: " + e.toString());
	} finally {
    	try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
        try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
        try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
	}
    
	result.put("retorno", retorno);
	return result;
		
    
};

public Connection getMSSQLSERVER(String strCallUUID, String servidorbd, String puerto, String usuario, String clave) throws SQLException {
	Logger log = Logger.getLogger("bk_UpdateRegistroDNI.jsp");
    String url = "jdbc:sqlserver://" + servidorbd + ":" + puerto + ";databaseName=international_data_store;user=" + usuario + ";password=" + clave + ";";
    Connection conexion = null;
    try {
    	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    	conexion = DriverManager.getConnection(url);
    } catch (SQLException e) {
    	setLog(strCallUUID + " IBCC-SBP :::: SQL Exception: " + e.toString());
    } catch (ClassNotFoundException cE) {
    	setLog(strCallUUID + " IBCC-SBP :::: Class Not Found Exception: " + cE.toString());
    } 
    return conexion;
}  

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>