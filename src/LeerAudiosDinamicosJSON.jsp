<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Element"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.util.*"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String strCallUUID = state.getString("CallUUID");
    setLog(strCallUUID + " IBCC-SBP :::: Lee JSON AudiosDinamicos.json");

    JSONObject result = new JSONObject();
    
    String audiosJSON = "";
    String ruta = "D:\\EmergenciaIVR\\SBP\\AudiosDinamicos.json";

    try {
      
      setLog(strCallUUID + " IBCC-SBP :::: Ruta JSON Audios Dinamicos = " + ruta);
      File fileJSON = new File(ruta);
      FileInputStream fis = new FileInputStream(fileJSON);
      BufferedReader br = new BufferedReader(new InputStreamReader(fis));

      StringBuilder sb = new StringBuilder(); 
      String line = "";

      while(( line = br.readLine()) != null ) {
        sb.append( line );
      }
      audiosJSON = sb.toString();

    } 
    catch(Exception e) {
        setLog(strCallUUID + " IBCC-SBP :::: Error Leer JSON = " + e.toString());
    }
    setLog(strCallUUID + " IBCC-SBP :::: JSON = " + audiosJSON);
    result.put("vAudiosStrJSON", audiosJSON);

    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>