<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.CallableStatement"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	//INPUT
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	String conexion = additionalParams.get("conexion");
	String tipoIVR = additionalParams.get("trama");
	
	String strOpcMP = "NO";
	String strOpcFin = "001";
	Calendar calendario = Calendar.getInstance();
	String anio, mes, dia, hora, minuto, segundo;
	
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bk_InsertarRegistroTrazabilidad.jsp");
	
	String tipoIVRArray[] = tipoIVR.split(",");
	
	String strTipoIVR = tipoIVRArray[0];
	String strTipoCliente = tipoIVRArray[1];
	
	setLog(strCallUUID + " IBCC-SBP :::: Tipo IVR: " + strTipoIVR);
	setLog(strCallUUID + " IBCC-SBP :::: Tipo Cliente: " + strTipoCliente);
	
	String conexionArray[] = conexion.split(",");
		
	String vServidor_bd = conexionArray[0];
	String vUsuario = conexionArray[1];
	String vClave = conexionArray[2];
	String vPuerto = conexionArray[3];
	
	setLog(strCallUUID + " IBCC-SBP :::: Servidor: " + vServidor_bd);
	setLog(strCallUUID + " IBCC-SBP :::: Puerto: " + vPuerto);
	
	setLog(strCallUUID + " IBCC-SBP :::: Opcion de Menu Principal: " + strOpcMP);
	setLog(strCallUUID + " IBCC-SBP :::: Ultima Opcion marcada: " + strOpcFin);
	
	// HORA y FECHA
	
	anio = Integer.toString(calendario.get(Calendar.YEAR));
	mes = Integer.toString(calendario.get(Calendar.MONTH)+1);
	dia = Integer.toString(calendario.get(Calendar.DAY_OF_MONTH));
	hora = Integer.toString(calendario.get(Calendar.HOUR_OF_DAY));
	minuto = Integer.toString(calendario.get(Calendar.MINUTE));
	segundo = Integer.toString(calendario.get(Calendar.SECOND));
	
	if(Integer.parseInt(mes)<10){
		mes = "0"+ mes;
	}
	
	if(Integer.parseInt(dia)<10){
		dia = "0"+ dia;
	}
	
	if(Integer.parseInt(hora)<10){
		hora = "0"+ hora;
	}
	
	if(Integer.parseInt(minuto)<10){
		minuto = "0"+ minuto;
	}
	
	if(Integer.parseInt(segundo)<10){
		segundo = "0"+ segundo;
	}
	
	String strFechaHora = anio + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;
	
	//OUTPUT
	String retorno = "0";
	
	JSONObject result = new JSONObject();
		
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	CallableStatement proc = null;
	
	setLog(strCallUUID + " IBCC-SBP :::: Fecha del Sistema: " + strFechaHora);
	setLog(strCallUUID + " IBCC-SBP :::: =============== BACKEND INSERTAR REGISTRO DE REPORTE DE TRAZABILIDAD ================================");
	
    try 
    {
    	setLog(strCallUUID + " IBCC-SBP :::: Iniciando Conexion SQL");
    	proc = getMSSQLSERVER(strCallUUID, vServidor_bd, vPuerto, vUsuario, vClave).prepareCall("{ call international_data_store.dbo.sp_insert_trazabilidad_llamadas(?,?,?,?,?,?) }",
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY, ResultSet.HOLD_CURSORS_OVER_COMMIT);

      	proc.setString(1, strCallUUID);
      	proc.setString(2, strFechaHora);
      	proc.setString(3, strTipoIVR);;
      	proc.setString(4, strTipoCliente);
      	proc.setString(5, strOpcMP);
      	proc.setString(6, strOpcFin);
      	
        setLog(strCallUUID + " IBCC-SBP :::: Iniciando ejecucion de SP - sp_insert_trazabilidad_llamadas");
        proc.executeUpdate();
        setLog(strCallUUID + " IBCC-SBP :::: Terminando ejecucion de SP - sp_insert_trazabilidad_llamadas");
    	setLog(strCallUUID + " IBCC-SBP :::: Codigo de retorno OK: " + retorno);
	} catch (Exception e) {
		setLog(strCallUUID + " IBCC-SBP :::: ERROR - SE EJECUTO INCORRECTAMENTE EL QUERY");
    	e.printStackTrace();
    	retorno = "-1";
    	setLog(strCallUUID + " IBCC-SBP :::: Codigo de retorno ERROR: " + retorno);
    	setLog(strCallUUID + " IBCC-SBP :::: Descripcion del ERROR: " + e.toString());
	} finally {
    	try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
        try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
        try { if (conn != null) conn.close(); } catch (SQLException e) { e.printStackTrace(); }
	}
    
	result.put("retorno", retorno);
	return result;
		
    
};

public Connection getMSSQLSERVER(String strCallUUID, String servidorbd, String puerto, String usuario, String clave) throws SQLException {
	Logger log = Logger.getLogger("bk_InsertarRegistroTrazabilidad.jsp");
    String url = "jdbc:sqlserver://" + servidorbd + ":" + puerto + ";databaseName=international_data_store;user=" + usuario + ";password=" + clave + ";";
    Connection conexion = null;
    try {
    	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    	conexion = DriverManager.getConnection(url);
    } catch (SQLException e) {
    	setLog(strCallUUID + " IBCC-SBP :::: SQL Exception: " + e.toString());
    } catch (ClassNotFoundException cE) {
    	setLog(strCallUUID + " IBCC-SBP :::: Class Not Found Exception: " + cE.toString());
    } 
    return conexion;
}  

%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Calendar"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>