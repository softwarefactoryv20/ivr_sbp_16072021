<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.io.File"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="javax.xml.parsers.DocumentBuilder"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.transform.Transformer"%>
<%@page import="javax.xml.transform.OutputKeys"%>
<%@page import="javax.xml.transform.TransformerException"%>
<%@page import="javax.xml.transform.TransformerFactory"%>
<%@page import="javax.xml.transform.dom.DOMSource"%>
<%@page import="javax.xml.transform.stream.StreamResult"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.w3c.dom.Element"%>

<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
    JSONObject result = new JSONObject();
    
    String numero           = additionalParams.get("ParamName2");
    String callUuid         = additionalParams.get("ParamName1");
    String telefono         = additionalParams.get("ParamName2");
    String numeroDestino    = additionalParams.get("ParamName3");
    
    String Path				= additionalParams.get("ParamName4");
    
    String xmlFilePath 		= "D:/TramaIVR/" + callUuid + ".XML";
    
    String U1   = "NULL";
    String U2   = "NULL";
    String U3   = "NULL";
    String U4   = "NULL";
    String U5   = "NULL";
    String U6   = "NULL";
    String U7   = "NULL";
    String U8   = "NULL";
    String U9   = "NULL";
    String U10  = "NULL";
    String U11  = "NULL";
    String U12  = "NULL";
    String U13  = "NULL";
    String U14  = "NULL";
    String U15  = "NULL";
    String U16  = "NULL";
    String U17  = "NULL";
    String U18  = "NULL";
    String U19  = "NULL";
    String U20  = "NULL";
    
    String CodigoPlataforma = "2";
    String nombreIVR        = "APP_IVR_PeruSBP";
    String dateformat    = "yyyy-MM-dd HH:mm:ss";
    SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
    String Fecha            = sdf.format(new Date());
    
    try {
    	

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        Element Call = document.createElement("Call");
        document.appendChild(Call);

        Element Information = document.createElement("Information");
        Call.appendChild(Information);

        Element Etelef  = document.createElement("telefono");Etelef.appendChild(document.createTextNode(telefono));
        Element EU1     = document.createElement("U1");EU1.appendChild(document.createTextNode(U1));
        Element EU2     = document.createElement("U2");EU2.appendChild(document.createTextNode(U2));
        Element EU3     = document.createElement("U3");EU3.appendChild(document.createTextNode(U3));
        Element EU4     = document.createElement("U4");EU4.appendChild(document.createTextNode(U4));
        Element EU5     = document.createElement("U5");EU5.appendChild(document.createTextNode(U5));
        Element EU6     = document.createElement("U6");EU6.appendChild(document.createTextNode(U6));
        Element EU7     = document.createElement("U7");EU7.appendChild(document.createTextNode(U7));
        Element EU8     = document.createElement("U8");EU8.appendChild(document.createTextNode(U8));
        Element EU9     = document.createElement("U9");EU9.appendChild(document.createTextNode(U9));
        Element EU10    = document.createElement("U10");EU10.appendChild(document.createTextNode(U10));
        Element EU11    = document.createElement("U11");EU11.appendChild(document.createTextNode(U11));
        Element EU12    = document.createElement("U12");EU12.appendChild(document.createTextNode(U12));
        Element EU13    = document.createElement("U13");EU13.appendChild(document.createTextNode(U13));
        Element EU14    = document.createElement("U14");EU14.appendChild(document.createTextNode(U14));
        Element EU15    = document.createElement("U15");EU15.appendChild(document.createTextNode(U15));
        Element EU16    = document.createElement("U16");EU16.appendChild(document.createTextNode(U16));
        Element EU17    = document.createElement("U17");EU17.appendChild(document.createTextNode(U17));
        Element EU18    = document.createElement("U18");EU18.appendChild(document.createTextNode(U18));
        Element EU19    = document.createElement("U19");EU19.appendChild(document.createTextNode(U19));
        Element EU20    = document.createElement("U20");EU20.appendChild(document.createTextNode(U20));
        Element EIvr    = document.createElement("ivr");EIvr.appendChild(document.createTextNode(CodigoPlataforma));
        Element EnomIVR = document.createElement("nombreIVR");EnomIVR.appendChild(document.createTextNode(nombreIVR));
        Element EAni    = document.createElement("ANI");EAni.appendChild(document.createTextNode(numero));
        Element EDNIS   = document.createElement("DNIS");EDNIS.appendChild(document.createTextNode(numeroDestino));
        Element EDate   = document.createElement("Date");EDate.appendChild(document.createTextNode(Fecha));
        Element Options = document.createElement("Options");EDate.appendChild(document.createTextNode(""));

        Information.appendChild(Etelef);
        Information.appendChild(EU1);
        Information.appendChild(EU2);
        Information.appendChild(EU3);
        Information.appendChild(EU4);
        Information.appendChild(EU5);
        Information.appendChild(EU6);
        Information.appendChild(EU7);
        Information.appendChild(EU8);
        Information.appendChild(EU9);
        Information.appendChild(EU10);
        Information.appendChild(EU11);
        Information.appendChild(EU12);
        Information.appendChild(EU13);
        Information.appendChild(EU14);
        Information.appendChild(EU15);
        Information.appendChild(EU16);
        Information.appendChild(EU17);
        Information.appendChild(EU18);
        Information.appendChild(EU19);
        Information.appendChild(EU20);
        Information.appendChild(EIvr);
        Information.appendChild(EnomIVR);
        Information.appendChild(EAni);
        Information.appendChild(EDNIS);
        Information.appendChild(EDate);
        Call.appendChild(Options);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(new File(xmlFilePath));
        
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        transformer.transform(domSource, streamResult);
        
    }catch(Exception e){
    }
    
    return result;
    
};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@include file="../include/backend.jspf" %>