<%@page language="java" contentType="application/json;charset=UTF-8" pageEncoding="UTF-8"%>
<%!
// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
        
    String ServidorBus = additionalParams.get("ServidorBus");
    String PuertoBus = additionalParams.get("PuertoBus");  
    String numeroDocumento = additionalParams.get("numeroDocumento");
    String carnetExtranjeria = additionalParams.get("vCE");
    String token = additionalParams.get("token");  
	//setLog(strCallUUID + " tOKEN PARA VALOR TARJETA: " + token );	
    String tempString = "";
	String valorTarjeta = "";
	String tipoDoc = "1";
	
	JSONObject result = new JSONObject();
    String strCallUUID = state.getString("CallUUID");
	
	if(carnetExtranjeria.length() > 0){
		tipoDoc = "2";
		numeroDocumento = carnetExtranjeria;
	}
	
    setLog(strCallUUID + " IBCC-SBP :::: BCK_WS_VALOR_TARJETA" );
    setLog(strCallUUID + " IBCC-SBP :::: numeroDocumento =" + numeroDocumento);
    setLog(strCallUUID + " IBCC-SBP :::: carnetExtranjeria =" + carnetExtranjeria);
    setLog(strCallUUID + " IBCC-SBP :::: tipoDoc =" + tipoDoc);	
    
    try{
		String url = "https://"+ServidorBus+":"+PuertoBus+"/gssbp-customer-fidelity/customer/indicator/cardValue/v1";
		//String url = "https://10.237.117.95:8143/gssbp-customer-fidelity/customer/indicator/cardValue/v1";
		String Body = "{\"country\": \"589\",\"documentNumber\": \""+numeroDocumento+"\",\"documentType\": \""+tipoDoc+"\"}";	
	    
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to URL = " + url);
		setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Body = " + Body);
		//setLog(strCallUUID + " IBCC-SBP :::: Sending 'POST' request to Token = " + token);
		/*
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
		
		setLog(strCallUUID + " IBCC-SBP :::: Install the all-trusting trust manager");
        
        // Install the all-trusting trust manager
        final SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        setLog(strCallUUID + " IBCC-SBP :::: Install the all-trusting host verifier");

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		
		setLog(strCallUUID + " IBCC-SBP :::: Start HttURLConnection");
		*/
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();						
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		//con.setRequestProperty("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJjb21wYW55Q29kZSI6IjEiLCJmaXJtYVVzdWFyaW8iOiIiLCJnbG9iYWxJZCI6IlM3ODM0OTQ5Iiwibm9tYnJlIjoiUEXDkUEgQ09CRcORQVMgWU9KQU5ZIE1BUklTT0wiLCJjb2RpZ29CVCI6Ik84NDMyNSIsInBlcmZpbCI6IkFHRU5URSIsImNvZGlnb0VtcHJlc2EiOiIiLCJjZHIiOiIwMDEwMyIsInB1ZXN0byI6IkFTRVNPUiBERSBFWFBFUklFTkNJQSBBTCBDTElFTlRFIiwiY29kaWdvRW1wbGVhZG8iOiI4NDMyNSIsImNvZGlnb1VzdWFyaW8iOiI4NDMyNSIsImNvcnJlbyI6IllPSkFOWS5QRU5BQFNDSS5DT00uUEUiLCJleHAiOjE2MTQ5ODE2NTMsImlhdCI6MTYxNDk3ODA1MywiY29kaWdvVmVuZGVkb3IiOiIifQ.fU6YcY33cMPMRFoh4cLfv_xJ5X0dxWQwlc775CdLbuaoujDizvfdzS47t0FJqQ0B_lNbJRvRdVgX6whVcL4-kQ");
		con.setRequestProperty("Authorization", token);
		byte[] input = Body.getBytes();
    	
    	OutputStream os = con.getOutputStream();
    	os.write(input,0,input.length);
    	
    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
    	
   		//print result
		tempString = response.toString();
		
		try{
			JSONObject jsonobj = new JSONObject(tempString);
			valorTarjeta = jsonobj.getString("cardValue");			
		}catch(JSONException Error){
			setLog(strCallUUID + " IBCC-SBP :::: Response Error Json = " + Error);
		}
    }catch(Exception Error){
		setLog(strCallUUID + " IBCC-SBP :::: Response Error = " + Error);
	}
		
		setLog(strCallUUID + " IBCC-SBP :::: Response Code = " + tempString);	
		setLog(strCallUUID + " IBCC-SBP :::: Response cardValue = " + valorTarjeta);	
	
		result.put("valorTarjeta", valorTarjeta);
	

    return result;

};
%>
<%-- GENERATED: DO NOT REMOVE --%> 
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONException"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.text.ParseException"%>

<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.Reader"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.cert.X509Certificate"%>
<%@page import="javax.net.ssl.HostnameVerifier"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="javax.net.ssl.SSLSession"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="javax.net.ssl.X509TrustManager"%>

<%@page import="org.apache.log4j.Logger"%>
<%@include file="../include/log/LogConfig.jspf" %>
<%@include file="../include/backend.jspf" %>

