function getDate(value)
{
	var resultString = "";

	var syear = value.substring(10, 6);
	var smonth = value.substring(5, 3);	
	var sday = value.substring(2, 0); 
	resultString = syear + smonth + sday;

	return resultString; 
}

function retPeriodo()
{
	var dat = new Date();
	var endResult = "";
	var sFullYear = "";
	var sDate = "";

	function prefiller(num) {
		return num < 10 ? '0' + num : num;
	}
	
	var sFullYear =  new String(dat.getFullYear());
	var sMonth = new String(prefiller(dat.getMonth()+1));
	
	endResult =	sFullYear + sMonth;
	return endResult; 
}


