/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspCServletContext/1.0
 * Generated at: 2020-03-14 00:26:10 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import middleware_support.tramas.Bean_IB87_IB88_IB93;
import middleware_support.classes.Saldo;
import IvrTransaction.RequestM3.PagoTarjetaCredito;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class Bck_005fIB87_005fIB88_005fIB93_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {



public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("Bck_IB87_IB88_IB93.jsp");
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruSBP/AudiosIBCC";
	
	//Input
	String vInstitucion=additionalParams.get("codigoInst");
	String vTarjeta= additionalParams.get("vNumDoc");
	String vCodProducto=additionalParams.get("codigoProducto");
	String vTipoCuenta=additionalParams.get("tipoCuenta");
	String vMonedaBT=additionalParams.get("monedaCuenta");
	String vTipoPago=additionalParams.get("tipoPago");
	String vMonedaPago=additionalParams.get("monedaPago");
	String vImportePagoCta=additionalParams.get("importePago");
	String vNroTarjeta=additionalParams.get("nroServicio");
	//Output
	int vCodErrorRpta = 0;
	String vCodRpta="0000"; 
	String codOperacion="";
	String saldodisponibleCargo="";
	
	Bean_IB87_IB88_IB93 refBean_IB87_IB88_IB93;
	JSONObject result = new JSONObject();
	
	vImportePagoCta=vImportePagoCta.replace("+", "");
	
	IvrTransaction.RequestM3.PagoTarjetaCredito modelo=new IvrTransaction.RequestM3.PagoTarjetaCredito();
	modelo.IvrPagoTarjetaCredito(vInstitucion, vTarjeta, vCodProducto, vTipoCuenta, vMonedaBT, vTipoPago, vMonedaPago, vImportePagoCta, vNroTarjeta);
	vCodErrorRpta=modelo.getCodRetorno();
		
	setLog(strCallUUID + " IBCC-SBP :::: ----------------- INVOCACION - IB87/IB88/IB93 : PAGO TARJETA DE CREDITO -------------------"); 
	if (vCodErrorRpta==0) {
		
		
		vCodRpta=modelo.getERROR();
		
		if(vCodRpta.equals("0000")){
			refBean_IB87_IB88_IB93=new Bean_IB87_IB88_IB93(url_audio, modelo, extension);
			codOperacion=refBean_IB87_IB88_IB93.getRELACIONBT();
			saldodisponibleCargo=refBean_IB87_IB88_IB93.getSALDISCTACARGO().getNumero();
			setLog(strCallUUID + " IBCC-SBP :::: ---------IB87/IB88------------");
			
			setLog(strCallUUID + " IBCC-SBP :::: Relacion BT : " + refBean_IB87_IB88_IB93.getRELACIONBT());
			setLog(strCallUUID + " IBCC-SBP :::: Saldo Disp Cta Cargo :  " + refBean_IB87_IB88_IB93.getSALDISCTACARGO().getNumero());
			setLog(strCallUUID + " IBCC-SBP :::: Signo Saldo Disp Cta Cargo : " + refBean_IB87_IB88_IB93.getSIGSALDODISP());
			setLog(strCallUUID + " IBCC-SBP :::: Saldo Contable Cta Cargo : " + refBean_IB87_IB88_IB93.getSALCONTCTACARGO().getNumero());
			setLog(strCallUUID + " IBCC-SBP :::: Signo Saldo Contable Cta Cargo : " + refBean_IB87_IB88_IB93.getSIGSALDOCONT());
			setLog(strCallUUID + " IBCC-SBP :::: Ind. Tipo de Cambio : " + refBean_IB87_IB88_IB93.getINDTIPCAMBIO());
			setLog(strCallUUID + " IBCC-SBP :::: Tipo de Cambio : " + refBean_IB87_IB88_IB93.getTIPOCAMBIO().getNumero());
			setLog(strCallUUID + " IBCC-SBP :::: Importe a Convertir : " + refBean_IB87_IB88_IB93.getIMPORTECONVERT().getNumero());
			setLog(strCallUUID + " IBCC-SBP :::: Signo Importe a Convertir : " + refBean_IB87_IB88_IB93.getSIGIMPORTCONV());
			setLog(strCallUUID + " IBCC-SBP :::: Moneda de Pago : " + refBean_IB87_IB88_IB93.getMONEDAPAGO());
			setLog(strCallUUID + " IBCC-SBP :::: Moneda de Tarjeta : " + refBean_IB87_IB88_IB93.getMONEDATARJETA());
			setLog(strCallUUID + " IBCC-SBP :::: Tipo de Tarjeta : " + refBean_IB87_IB88_IB93.getTIPOTARJETA());
			setLog(strCallUUID + " IBCC-SBP :::: Importe Pago Cuenta : " + refBean_IB87_IB88_IB93.getIMPORTEPAGCTA().getNumero());
			setLog(strCallUUID + " IBCC-SBP :::: Signo Importe Pago Cuenta : " + refBean_IB87_IB88_IB93.getSIGPAGOCTA());
			setLog(strCallUUID + " IBCC-SBP :::: Importe de Cargo : " + refBean_IB87_IB88_IB93.getIMPORTECARGO().getNumero());
			setLog(strCallUUID + " IBCC-SBP :::: Moneda de Cargo : " + refBean_IB87_IB88_IB93.getMONEDACARGO());
			setLog(strCallUUID + " IBCC-SBP :::: Signo Importe de Cargo : " + refBean_IB87_IB88_IB93.getSIGIMPORTCARGADO());
			setLog(strCallUUID + " IBCC-SBP :::: Procesadora : " + refBean_IB87_IB88_IB93.getPROCESADORA());
					
			setLog(strCallUUID + " IBCC-SBP :::: ---------IB93------------");
			
			setLog(strCallUUID + " IBCC-SBP :::: Mas datos : " + refBean_IB87_IB88_IB93.getRefBean_IB93().getMasDatos());
			setLog(strCallUUID + " IBCC-SBP :::: Nro Sgte. Pagina : " + refBean_IB87_IB88_IB93.getRefBean_IB93().getNroSiguentePagina());
			setLog(strCallUUID + " IBCC-SBP :::: Saldos");
			
			setLog(strCallUUID + " IBCC-SBP :::: N°  TipoCta  CodSucursal  NombreSucursal Mon.  Cod.Prod. Ind.Banco SaldoContable Signo SaldoDisp Signo Tasa       Signo Fecha        Desc.        Cod CCI Ind-Afil");
			
			int i=1;
			
			for(Saldo refSaldo: refBean_IB87_IB88_IB93.getRefBean_IB93().getListaSaldosCTAS()){
					
				setLog(strCallUUID + " IBCC-SBP :::: " + i+"    "+
						refSaldo.getTipoCta()+"         "+
						refSaldo.getCodigoSucursal()+"         "+
						refSaldo.getNombreSucursal()+"       "+
						refSaldo.getMonedaBT()+"    "+
						refSaldo.getCodigoProducto()+"    "+
						refSaldo.getIndicadorBanco()+"    "+
						refSaldo.getSaldoContable().getNumero()+"    "+
						refSaldo.getSignoSaldoContable()+"    "+
						refSaldo.getSaldoDisponible().getNumero()+"    "+
						refSaldo.getSignoSalDisponible()+"    "+
						refSaldo.getTasaIntereses().getNumero()+"    "+
						refSaldo.getSignoTasaIntereses()+"   "+
						refSaldo.getFechaApertura().getFecha()+"    "+
						refSaldo.getDescripcionEstado()+"    "+
						refSaldo.getCodigoCCI()+"    "+
						refSaldo.getIndicadorAfiliacion()+"");
				i++;
			}	
			
			
			
		}
		
		
	}
	
	setLog(strCallUUID + " IBCC-SBP :::: Codigo Retorno Host Disponible = " + vCodErrorRpta);
	setLog(strCallUUID + " IBCC-SBP :::: Codigo Retorno Transaccion OK = " + vCodRpta);
	
	
	result.put("vCodErrorRpta", Integer.toString(vCodErrorRpta));
	result.put("vCodRpta", vCodRpta);
	result.put("codOperacion", codOperacion);
	result.put("saldoDispCargo", saldodisponibleCargo);
	
	return result;
		
    
};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;

		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(2);
    _jspx_dependants.put("/src/../include/backend.jspf", Long.valueOf(1374495838000L));
    _jspx_dependants.put("/src/../include/log/LogConfig.jspf", Long.valueOf(1561680621979L));
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
