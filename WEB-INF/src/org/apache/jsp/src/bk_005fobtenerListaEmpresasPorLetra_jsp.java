/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: JspCServletContext/1.0
 * Generated at: 2020-03-14 00:26:18 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.src;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import middleware_support.support.MiddlewareValue;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletContext;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.Map;
import org.json.JSONObject;
import com.genesyslab.studio.backendlogic.GVPHttpRequestProcessor;
import java.util.Map;

public final class bk_005fobtenerListaEmpresasPorLetra_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


// Implement this method to execute some server-side logic.
public JSONObject performLogic(JSONObject state, Map<String, String> additionalParams) throws Exception {
    
	String strCallUUID = state.getString("CallUUID") + "-" + additionalParams.get("nc_ANI");
	//sesion =  strCallUUID + ", ";
	Logger log = Logger.getLogger("bk_obtenerListaEmpresasPorLetra.jsp");
	//INPUT
	String extension=".wav";
	String servidorwas = additionalParams.get("servidorwas");
	String url = "http://" + servidorwas + ":8080";
	String url_audio = url + "/APP_IVR_PeruSBP/AudiosIBCC";
	
	String vLetraInicial = additionalParams.get("letraInicial");
	String vListaEmpresas = additionalParams.get("listaEmpresas");
	String vListaCodNegocio = additionalParams.get("listaCodNegocio");
	
	
	//OUTPUT
	String vListaAudiosEmpresasString="";
	String vListaCodNegocioString="";
	String vNumEmpresas="";
	
	JSONObject result = new JSONObject();
		
	List<String> listaCodNegocioIn = MiddlewareValue.getUrlsFromJsonString(vListaCodNegocio);
	List<String> listaEmpresas= MiddlewareValue.getUrlsFromJsonString(vListaEmpresas);
	
	List<Character> listaLetras=new ArrayList();
	List<String> listaCodNegocioOut=new ArrayList();
	List<String> listaAudiosEmpresas=new ArrayList();
	
	setLog(strCallUUID + " IBCC-SBP :::: =============== BACKEND OBTENER LISTA EMPRESAS POR LETRA ================================");
	
	switch(Integer.parseInt(vLetraInicial)){
	
		case 2: listaLetras.add('A');
				listaLetras.add('B');
				listaLetras.add('C');
				break;
		case 3: listaLetras.add('D');
				listaLetras.add('E');
				listaLetras.add('F');
				break;
		case 4: listaLetras.add('G');
				listaLetras.add('H');
				listaLetras.add('I');
				break;
		case 5: listaLetras.add('J');
				listaLetras.add('K');
				listaLetras.add('L');
				break;
		case 6: listaLetras.add('M');
				listaLetras.add('N');
				listaLetras.add('O');
				break;
		
		case 7: listaLetras.add('P');
				listaLetras.add('Q');
				listaLetras.add('R');
				listaLetras.add('S');
				break;
		case 8: listaLetras.add('T');
				listaLetras.add('U');
				listaLetras.add('V');
				break;
		case 9: listaLetras.add('W');
				listaLetras.add('X');
				listaLetras.add('Y');
				listaLetras.add('Z');
				break;

	}
	
	for(char letra: listaLetras){
		
		int i=0;
		for(String empresa: listaEmpresas){
			
			if(empresa.toUpperCase().charAt(0)==letra){
				String codNegocio=listaCodNegocioIn.get(i);
				listaAudiosEmpresas.add(url_audio+"/"+empresa+extension);
				listaCodNegocioOut.add(codNegocio);
				
			}
			i++;
	    
		}
	}
	
	
    vNumEmpresas=Integer.toString(listaAudiosEmpresas.size());
    vListaAudiosEmpresasString=MiddlewareValue.getListaString(listaAudiosEmpresas);
    vListaCodNegocioString=MiddlewareValue.getListaString(listaCodNegocioOut);
    
	
    setLog(strCallUUID + " IBCC-SBP :::: Num Empresas = "+vNumEmpresas);
    setLog(strCallUUID + " IBCC-SBP :::: Lista Audios Empresas = "+vListaAudiosEmpresasString);
    setLog(strCallUUID + " IBCC-SBP :::: Lista Cod Negocio = "+vListaCodNegocioString);
	    
    
	
	result.put("numEmpresas", vNumEmpresas);
	result.put("listaAudiosEmpresas", vListaAudiosEmpresasString);
	result.put("listaCodNegocio", vListaCodNegocioString);
	return result;	
    
};


	String rutaLog = "";
	String sesion =  "";



	public String getPath(){
		return rutaLog;
	}

	public void setLog(String objetivo) {
		String rutaApp = rutaLog;

		String namelog = "";
		String dateformat = "yyyy-MM-dd";
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdf = null;
		try {
			if(!objetivo.equals("")){

				rutaApp += "/webapps/Log";
				
				File f = new File(rutaApp);
				if (!f.exists()) {
					f.mkdirs();
				}
				sdf = new SimpleDateFormat(dateformat);
				rutaApp += "/"+ namelog + sdf.format(new Date()) + ".txt";
				
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
				String vars[] = objetivo.split("\n");
				for(String i: vars){
					sb.append(sesion + sdf.format(new Date()) + " : " + i.trim() + "\r\n");
				}
				BufferedWriter bw = new BufferedWriter(new FileWriter(rutaApp,true));
				bw.write(sb.toString());
				bw.flush();
				bw.close();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(2);
    _jspx_dependants.put("/src/../include/backend.jspf", Long.valueOf(1374495838000L));
    _jspx_dependants.put("/src/../include/log/LogConfig.jspf", Long.valueOf(1561680621979L));
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("application/json;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, false, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write(" \r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');

	ServletContext servletContext = request.getSession().getServletContext();
	rutaLog = servletContext.getRealPath("/WEB-INF/web.xml");
	rutaLog = rutaLog.substring(0,rutaLog.indexOf("\\WEB-INF"));

      out.write("\r\n\r\n\r\n");
      out.write("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
      out.write('\r');
      out.write('\n');
      out.write("\r\n\r\n\r\n\r\n\r\n");

response.setHeader("Cache-Control", "no-cache");

String output = null;

try {
    // process the post data
    GVPHttpRequestProcessor processor = new GVPHttpRequestProcessor(request);
    processor.parseRequest();
    
    // "state" encapsulates the state variable submitted by the VXML page
    JSONObject state = processor.getState();
    
    // additional parameters that were passed in the namelist
    Map<String, String> additionalParams = processor.getAdditionalParams();
    
    // perform the logic
    JSONObject result = performLogic(state, additionalParams);
    
	output = result.toString();
    
    out.print(output);
    
} catch (Exception e) {
    
    e.printStackTrace();
    String msg = e.getMessage();
    if (null != msg){
    	msg = msg.replace('"', '\'');
    }
	out.print("{\"errorMsg\": \"" + msg + "\"}");
	
}

    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
